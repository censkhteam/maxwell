extern crate maxwell;

use maxwell::compiler::transform::PluginManager;
use maxwell::compiler::{Chunk, Compiler,CompilerConfig};
use maxwell::compiler::parser::{JsParser, ParserOptions,Parser};
use maxwell::compiler::output::MemoryOutput;
use maxwell::compiler::ast::Node;

#[test]
fn it_works() {
    let mut chunk = Chunk::from_source(String::from("_test"), String::from("function test() {}"));

    let mut parser = JsParser::new();
    let result = parser.parse(ParserOptions::new(&mut chunk));

    println!("{:#?}",result);
    assert!(result.is_ok() && result.unwrap().syntax_tree.generate().len() > 0);
}