use js::StatementNode;

use core::transform::PluginPass;
use core::parser::{Node, NodeList, SourceLocation, SourceRange};
use core::output::CompilerOutput;

use std::ops::DerefMut;

#[derive(Debug, Clone, PartialEq)]
pub struct BodyNode {
    pub content: Vec<StatementNode>,
    range: SourceRange,
}

impl BodyNode {
    pub fn new(content: Vec<StatementNode>) -> Self {
        let range = SourceRange::new(
            content.first().unwrap().range().start,
            content.last().unwrap().range().end
        );
        return BodyNode {
            range,
            content,
        };
    }
}

impl Node for BodyNode {
    fn generate(&self, output: &mut Box<CompilerOutput>) {
        for statement in &self.content {
            statement.generate(output);
        }
    }

    fn children<'a>(&'a mut self) -> Option<NodeList<'a>> {
        let mut children: NodeList = Vec::new();
        for child in &mut self.content {
            children.push(child);
        }
        return Some(children);
    }

    fn plugin_passes(&mut self) -> Vec<PluginPass> {
        return vec![];
    }

    fn range<'a>(&'a self) -> &'a SourceRange {
        return &self.range;
    }
}