use core::parser::{NodeTrivia, SourceRange, Node, NodeList, QuoteKind};
use core::transform::PluginPass;
use core::output::CompilerOutput;

use js::{ExpressionNode, Expression, DeclarationKind, DeclarationNode, CommentKind};

#[derive(Debug, PartialEq, Clone)]
pub struct ImportTrivia {
    pub declaration_prefix: String,
    pub as_prefix: String,
    pub alias_prefix: String,
    pub from_prefix: String,
    pub source_prefix: String,
    pub quote_kind: QuoteKind,
}

impl ImportTrivia {
    pub fn new() -> Self {
        return ImportTrivia {
            declaration_prefix: String::new(),
            as_prefix: String::new(),
            from_prefix: String::new(),
            alias_prefix: String::new(),
            source_prefix: String::new(),
            quote_kind: QuoteKind::Apostrophe,
        };
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum ImportDeclaration {
    All,
    Single(String),
    Multiple(Vec<String>)
}

#[derive(Debug, PartialEq, Clone)]
pub enum Statement {
    Uncompiled {
        content: String
    },
    Expression {
        expression: ExpressionNode
    },
    Declaration {
        kind: DeclarationKind,
        declarations: Vec<DeclarationNode>
    },
    Return {
        expression: Option<ExpressionNode>
    },
    Import {
        alias: Option<String>,
        declaration: ImportDeclaration,
        source: String,
        trivia: ImportTrivia
    },
    Comment {
        kind: CommentKind,
        content: String
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum StatementTerminator {
    Semicolon,
    Newline,
    Block,
    None,
}

#[derive(Debug, PartialEq, Clone)]
pub struct StatementNode {
    pub statement: Statement,
    range: SourceRange,
    pub trivia: NodeTrivia,
    pub terminator: StatementTerminator,
    pub requires: Vec<String>,
}

impl StatementNode {
    pub fn new(statement: Statement, trivia: NodeTrivia, terminator: StatementTerminator, range: SourceRange) -> Self {
        return StatementNode {
            statement,
            range,
            trivia,
            terminator,
            requires: Vec::new(),
        };
    }
}

impl Node for StatementNode {
    fn generate(&self, output: &mut Box<CompilerOutput>) {
        use self::Statement::*;

        output.write_string(&self.trivia.prefix);

        match self.statement {
            Declaration { ref kind, ref declarations } => {
                output.write_string(&kind.to_string());
                output.write_string(" ");
                for i in 0..declarations.len() {
                    if i > 0 {
                        output.write_string(",");
                    }
                    declarations[i].generate(output);
                }
            }
            Comment { ref kind, ref content } => {
                output.write_string(match *kind {
                    CommentKind::Line => "//",
                    CommentKind::Body => "/*",
                });
                output.write_string(content);
                if *kind == CommentKind::Body {
                    output.write_string("*/");
                }
            }
            Uncompiled { ref content } => {
                output.write_string(content);
            }
            Expression { ref expression } => {
                expression.generate(output);
            }
            Return { ref expression } => {
                output.write_string("return");
                match expression {
                    &Some(ref value) => {
                        value.generate(output);
                    }
                    _ => {}
                }
            }
            Import { ref alias, ref declaration, ref source, ref trivia } => {
                output.write_string("import");
                output.write_string(&trivia.declaration_prefix);

                match declaration {
                    &ImportDeclaration::All => output.write_string("*"),
                    &ImportDeclaration::Single(ref name) => output.write_string(name),
                    &ImportDeclaration::Multiple(ref names) => output.write_string(&names.join(","))
                };

                match alias {
                    &Some(ref name) => {
                        output.write_string(&trivia.as_prefix);
                        output.write_string("as");
                        output.write_string(&trivia.alias_prefix);
                        output.write_string(name);
                    }
                    _ => {}
                }

                output.write_string(&trivia.from_prefix);
                output.write_string("from");
                output.write_string(&trivia.source_prefix);
                output.write_string(&trivia.quote_kind.to_string());
                output.write_string(source);
                output.write_string(&trivia.quote_kind.to_string());
            }
        };

        output.write_string(&self.trivia.suffix);
        output.write_string(match self.terminator {
            StatementTerminator::Semicolon => ";",
            StatementTerminator::Newline => "\n",
            _ => ""
        })
    }

    fn plugin_passes(&mut self) -> Vec<PluginPass> {
        return vec![PluginPass::StatementNode(self)];
    }

    fn children<'a>(&'a mut self) -> Option<NodeList<'a>> {
        use self::Statement::*;

        //TODO: Optimize
        return match self.statement {
            Expression { ref mut expression, .. } => Some(vec![expression]),
            Declaration { ref mut declarations, ..} => {
                let mut nodes : NodeList<'a> = Vec::new();
                for decl in declarations.iter_mut() {
                    nodes.push(decl);
                }
                Some(nodes)
            }
            Return { ref mut expression, .. } => match expression {
                &mut Some(ref mut node) => Some(vec![node]),
                _ => None
            },
            _ => None
        };
    }
    fn range<'a>(&'a self) -> &'a SourceRange {
        return &self.range;
    }
}