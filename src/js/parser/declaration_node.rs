use js::ExpressionNode;

use core::output::CompilerOutput;
use core::parser::{Node, NodeList, SourceRange};
use core::transform::PluginPass;

use std::slice::IterMut;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct DeclarationTrivia {
    pub assign_prefix: String
}

impl DeclarationTrivia {
    pub fn new(assign_prefix : String) -> Self {
        return DeclarationTrivia { assign_prefix};
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct DeclarationNode {
    pub name: String,
    pub expression: Option<ExpressionNode>,
    pub trivia: DeclarationTrivia,
    range: SourceRange,
}

#[derive(Debug, PartialEq, Clone)]
pub enum DeclarationKind {
    Let,
    Const,
    Var,
}

impl DeclarationNode {
    pub fn new(name: String, expression: Option<ExpressionNode>, trivia: DeclarationTrivia, range: SourceRange) -> Self {
        return DeclarationNode { name, expression, trivia, range };
    }
}

impl Node for DeclarationNode {
    fn generate(&self, output: &mut Box<CompilerOutput>) {
        output.write_string(&self.name);

        match self.expression {
            Some(ref expression_node) => {
                output.write_string(&self.trivia.assign_prefix);
                output.write_string("=");
                expression_node.generate(output);
            }
            None => {}
        }
    }

    fn plugin_passes(&mut self) -> Vec<PluginPass> {
        return vec![];
    }

    fn children<'a>(&'a mut self) -> Option<NodeList<'a>> {
        return match self.expression {
            Some(ref mut expression) => Some(vec![expression]),
            _ => None,
        };
    }
    fn range<'a>(&'a self) -> &'a SourceRange {
        return &self.range;
    }
}

impl ToString for DeclarationKind {
    fn to_string(&self) -> String {
        use self::DeclarationKind::*;

        return match self {
            &Const => "const",
            &Let => "let",
            &Var => "var",
        }.to_owned();
    }
}