#[derive(Clone, Debug, PartialEq)]
pub enum CommentKind {
    Line,
    Body
}