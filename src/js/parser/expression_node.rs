use js::{Literal, BodyNode, Parameter, OperatorKind};

use core::transform::PluginPass;
use core::parser::{SourceRange, NodeTrivia, Node, NodeList};
use core::output::CompilerOutput;

use std::ops::DerefMut;
use std::slice::IterMut;

#[derive(Debug, PartialEq, Clone)]
pub struct FunctionTrivia {
    pub identifier_gap: String,
    pub parameters_gap: String,
    pub body_gap: String,
    pub parameters_padding: String,
    pub body_suffix: String,
}

impl FunctionTrivia {
    pub fn default() -> Self {
        return FunctionTrivia {
            body_suffix: String::new(),
            identifier_gap: String::new(),
            parameters_gap: String::new(),
            body_gap: String::new(),
            parameters_padding: String::new()
        };
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct ArrowFunctionTrivia {
    pub parameters_gap: String,
    pub arrow_gap: String,
    pub body_gap: String,
    pub parameters_padding: String,
    pub body_suffix: String,
}

impl ArrowFunctionTrivia {
    pub fn default() -> Self {
        return ArrowFunctionTrivia {
            body_suffix: String::new(),
            arrow_gap: String::new(),
            parameters_gap: String::new(),
            body_gap: String::new(),
            parameters_padding: String::new()
        };
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Expression {
    Void,
    This,
    Bracketed {
        expression: Box<ExpressionNode>
    },
    Function {
        name: String,
        parameters: Vec<Parameter>,
        body: BodyNode,
        trivia: FunctionTrivia
    },
    ArrowFunction {
        parameters: Vec<Parameter>,
        body: BodyNode,
        trivia: ArrowFunctionTrivia
    },
    Call {
        identifier: String,
        parameters: Vec<ExpressionNode>
    },
    Operation {
        operator: OperatorKind,
        left_hand: Box<ExpressionNode>,
        right_hand: Box<ExpressionNode>
    },
    Identifier(String),
    Literal(Literal)
}

#[derive(Debug, PartialEq, Clone)]
pub struct ExpressionNode {
    pub expression: Expression,
    range: SourceRange,
    pub trivia: NodeTrivia,
}

impl ExpressionNode {
    pub fn new(expression: Expression, trivia: NodeTrivia, range: SourceRange) -> Self {
        return ExpressionNode {
            expression,
            range,
            trivia
        };
    }
}

impl Node for ExpressionNode {
    fn generate(&self, output: &mut Box<CompilerOutput>) {
        use self::Expression::*;

        output.write_string(&self.trivia.prefix);
        match self.expression {
            //TODO: Cleanup
            Function { ref name, ref parameters, ref body, ref trivia } => {
                output.write_string("function");
                output.write_string(&trivia.identifier_gap);
                output.write_string(name);
                output.write_string(&trivia.parameters_gap);
                output.write_string("(");
                output.write_string(&trivia.parameters_padding);

                for i in 0..parameters.len() {
                    if i > 0 {
                        output.write_string(",");
                    }
                    let param = &parameters[i];
                    output.write_string(&param.trivia.prefix);
                    output.write_string(&param.name);
                    output.write_string(&param.trivia.suffix);
                }

                output.write_string(")");
                output.write_string(&trivia.body_gap);
                output.write_string("{");

                body.generate(output);

                output.write_string(&trivia.body_suffix);
                output.write_string("}");
            }
            Call { ref identifier, ref parameters } => {
                output.write_string(identifier);
                output.write_string("(");

                for i in 0..parameters.len() {
                    if i > 0 {
                        output.write_string(",");
                    }
                    let expression = &parameters[i];
                    expression.generate(output);
                }

                output.write_string(")");
            }
            Bracketed { ref expression } => {
                output.write_string("(");
                expression.generate(output);
                output.write_string(")");
            }
            This => {
                output.write_string("this")
            }
            Identifier(ref string) => {
                output.write_string(string)
            }
            Operation {ref left_hand, ref right_hand, ref operator} => {
                left_hand.generate(output);
                output.write_string(&operator.to_string());
                right_hand.generate(output);
            }
            Literal(ref literal) => { output.write_string(&literal.to_string()) }
            _ => {}
        };
        output.write_string(&self.trivia.suffix);
    }

    fn children<'a>(&'a mut self) -> Option<NodeList<'a>> {
        use self::Expression::*;

        return match self.expression {
            Bracketed { ref mut expression } => {
                let mut nodes: NodeList = Vec::new();
                nodes.push(expression.deref_mut());
                return Some(nodes);
            }
            Call { ref mut parameters, .. } => {
                let mut nodes: NodeList = Vec::new();
                for param in parameters {
                    nodes.push(param);
                }
                return Some(nodes);
            }
            Function { ref mut body, .. } => Some(vec![body]),
            Operation { ref mut left_hand, ref mut right_hand, .. } => {
                return Some(vec![left_hand.deref_mut(), right_hand.deref_mut()]);
            }
            _ => None,
        };
    }

    fn plugin_passes(&mut self) -> Vec<PluginPass> {
        return vec![PluginPass::ExpressionNode(self)];
    }
    fn range<'a>(&'a self) -> &'a SourceRange {
        return &self.range;
    }
}