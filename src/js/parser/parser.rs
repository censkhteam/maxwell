use core::parser::*;
use core::{CompilerError, CompilerErrorKind, ChunkId, Chunk};
use core::transform::PluginPass;
use core::lexer::{BaseTokenKind, TokenKind};
use core::log::Message;

use js::*;

use std::time::Instant;

type JsParserSession<'a> = ParserSession<'a, JsTokenKind>;

#[derive(Debug)]
pub struct JsParser {}

impl Parser for JsParser {
    fn parse(&self, chunk: &mut Chunk) -> Result<ParserResult, ParserError> {
        use self::JsTokenKind::*;

        let start = Instant::now();
        let mut session = ParserSession::new(chunk, Box::new(JsTokenizer::new()));
        session.chunk.index = 0;

        let mut contents = Vec::new();

        loop {
            let token = match session.tokenizer.peek_token(session.chunk) {
                Ok(token) => token,
                Err(err) => {
                    session.log.add_message(session.chunk.id(), Box::new(ParserError::from(err)));
                    session.tokenizer.pop_token(session.chunk)?;
                    continue;
                }
            };
            if token.kind() == Base(BaseTokenKind::EndOfFile) {
                break;
            }
            let start = token.start;
            match self.parse_statement(&mut session) {
                Ok(statement_node) => contents.push(statement_node),
                Err(err) => {
                    session.log.add_message(session.chunk.id(), Box::new(err));
                    session.tokenizer.pop_token(session.chunk)?;
                    let content = session.chunk.slice(start, session.chunk.index).to_owned();
                    contents.push(StatementNode::new(
                        Statement::Uncompiled { content },
                        NodeTrivia::default(),
                        StatementTerminator::None,
                        SourceRange::new(session.source_location(start), session.current_source_location())
                    ));
                }
            }
        }
        session.chunk.syntax_tree = Some(SyntaxTree::new(Box::new(BodyNode::new(contents))));
        return Ok(ParserResult::new(session, start.elapsed()));
    }

    fn query_extension(&self, string: &str) -> bool {
        return string.eq("js");
    }

    fn name(&self) -> &'static str {
        return "js";
    }
}

impl JsParser {
    pub fn new() -> Self {
        return JsParser {};
    }

    fn parse_function(&self, session: &mut JsParserSession, range: &mut SourceRange) -> Result<(Expression, Option<StatementTerminator>), ParserError> {
        use self::JsTokenKind::*;

        let mut trivia = FunctionTrivia::default();

        let identifier_token = session.tokenizer.pop_ignore_padding(session.chunk)?;
        let identifier = match identifier_token.kind() {
            Identifier(ref name) => name.to_owned(),
            _ => return Err(ParserError::new(ParserErrorKind::Syntax(String::from("Function has no identifier.")), identifier_token.range(session.chunk)))
        };
        trivia.identifier_gap = identifier_token.prefix.clone();

        let bracket_token = session.tokenizer.pop_ignore_padding(session.chunk)?;
        match bracket_token.kind() {
            BracketOpen => {}
            _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Function params must begin with bracket")), bracket_token.range(session.chunk)))
        }
        trivia.parameters_gap = bracket_token.prefix.clone();

        let mut parameters = Vec::new();
        loop {
            let parameter_token = session.tokenizer.pop_ignore_padding(session.chunk)?;
            match parameter_token.kind() {
                BracketClose => {
                    if parameters.len() == 0 {
                        trivia.parameters_padding += &parameter_token.prefix.clone();
                        break;
                    }
                    return Err(ParserError::new(ParserErrorKind::Syntax(format!("Unexpected end of params")), parameter_token.range(session.chunk)));
                }
                Identifier(ref ident) => {
                    let next_token = session.tokenizer.pop_ignore_padding(session.chunk)?;

                    parameters.push(Parameter {
                        name: ident.to_owned(),
                        default: None,
                        trivia: ParameterTrivia { prefix: parameter_token.prefix.clone(), suffix: next_token.prefix.clone() }
                    });

                    match next_token.kind() {
                        Comma => continue,
                        BracketClose => break,
                        _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Function params contains invalid token")), next_token.range(session.chunk))),
                    }
                }
                _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid function")), parameter_token.range(session.chunk))),
            }
        }

        let body_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
        if body_token.kind() != BraceOpen {
            return Err(ParserError::new(ParserErrorKind::Syntax(format!("Function body does not end")), body_token.range(session.chunk)));
        }
        session.tokenizer.pop_ignore_padding(session.chunk)?;
        trivia.body_gap = body_token.prefix.clone();

        let mut body = Vec::new();
        loop {
            let token = session.tokenizer.peek_ignore_padding(session.chunk)?;
            match token.kind() {
                BraceClose => {
                    trivia.body_suffix = token.prefix.clone();
                    range.end = session.source_location(token.end);
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                    break;
                }
                _ => {
                    let mut statement_node = self.parse_statement(session)?;
                    statement_node.trivia.prefix = token.prefix.clone();
                    body.push(statement_node)
                }
            }
        }

        return Ok((Expression::Function {
            name: identifier,
            parameters,
            body: BodyNode::new(body),
            trivia
        }, Some(StatementTerminator::Block)));
    }

    fn parse_bracket_expression(&self, session: &mut JsParserSession, range: &mut SourceRange) -> Result<Expression, ParserError> {
        use self::JsTokenKind::*;

        let mut expressions = Vec::new();

        loop {
            let token = session.tokenizer.peek_ignore_padding(session.chunk)?;
            match token.kind() {
                BracketClose => {
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                    range.end = session.source_location(token.end);
                    break;
                }
                Comma => {
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                }
                _ => {
                    let (child_expression, _) = self.parse_expression(session)?;
                    expressions.push(child_expression);
                }
            }
        }
        let possible_fat_arrow = session.tokenizer.peek_ignore_padding(session.chunk)?;
        if possible_fat_arrow.kind() == FatArrow {
            return Err(ParserError::new(ParserErrorKind::Unimplemented, possible_fat_arrow.range(session.chunk)));
        } else {
            let expression = expressions.remove(0);
            return Ok(Expression::Bracketed { expression: Box::new(expression) });
        }
    }


    fn parse_expression(&self, session: &mut JsParserSession) -> Result<(ExpressionNode, Option<StatementTerminator>), ParserError> {
        let mut range = SourceRange::default();
        let mut trivia = NodeTrivia::default();
        let mut expression_option: Option<Expression> = None;
        let mut terminator_option: Option<StatementTerminator> = None;

        let token = session.tokenizer.peek_ignore_padding(session.chunk)?;
        range.start = session.source_location(token.start);
        range.end = session.source_location(token.end);

        trivia.prefix = token.prefix.clone();
        match token.kind() {
            JsTokenKind::Keyword(Keyword::This) => {
                expression_option = Some(Expression::This);
                range.end = session.source_location(token.end);
            },
            JsTokenKind::BracketOpen => {
                session.tokenizer.pop_ignore_padding(session.chunk)?;
                match self.parse_bracket_expression(session, &mut range) {
                    Ok(expression) => {
                        expression_option = Some(expression);
                    }
                    Err(err) => return Err(err)
                }
            }
            JsTokenKind::Literal(ref literal) => {
                expression_option = Some(Expression::Literal(literal.clone()));
                range.end = session.source_location(token.end);
                session.tokenizer.pop_ignore_padding(session.chunk)?;
            }
            JsTokenKind::Keyword(keyword) => {
                match keyword {
                    Keyword::Function => {
                        session.tokenizer.pop_ignore_padding(session.chunk)?;
                        match self.parse_function(session, &mut range) {
                            Ok((expression, terminator)) => {
                                expression_option = Some(expression);
                                terminator_option = terminator;
                            }
                            Err(err) => return Err(err)
                        }
                    }
                    _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Keyword '{}' not supported.", keyword)), token.range(session.chunk)))
                }
            }
            JsTokenKind::Identifier(ref name) => {
                session.tokenizer.pop_ignore_padding(session.chunk)?;
                let next_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
                match next_token.kind() {
                    JsTokenKind::BracketOpen => {
                        session.tokenizer.pop_ignore_padding(session.chunk)?;
                        match self.parse_call(session, name.to_owned(), &mut range) {
                            Ok((expression, terminator)) => {
                                expression_option = Some(expression);
                                terminator_option = terminator;
                            }
                            Err(err) => return Err(err)
                        }
                    }
                    _ => {
                        range.end = session.source_location(token.end);
                        expression_option = Some(Expression::Identifier(name.to_owned()));
                    }
                }
            }
            _ => {}
        }

        if expression_option == None {
            let token = session.tokenizer.peek_token(session.chunk)?;
            return Err(ParserError::new(
                ParserErrorKind::Syntax(format!("Expression could not be parsed")),
                token.range(session.chunk))
            );
        }

        let mut expression_node = ExpressionNode::new(expression_option.unwrap(), trivia, range);
        let token = session.tokenizer.peek_ignore_padding(session.chunk)?;
        match token.kind() {
            JsTokenKind::Operator(ref operator_kind) => {
                expression_node.trivia.suffix = token.prefix;
                session.tokenizer.pop_ignore_padding(session.chunk)?;
                let (other_expression, _) = self.parse_expression(session)?;
                let range = SourceRange::new(expression_node.range().start, other_expression.range().end);
                return Ok((ExpressionNode::new(Expression::Operation {
                    operator: operator_kind.clone(),
                    left_hand: Box::new(expression_node),
                    right_hand: Box::new(other_expression)
                }, NodeTrivia::default(), range), None));
            }
            _ => {}
        }

        return Ok((expression_node, terminator_option));
    }

    fn parse_call(&self, session: &mut JsParserSession, identifier: String, range: &mut SourceRange) -> Result<(Expression, Option<StatementTerminator>), ParserError> {
        let mut parameters = Vec::new();
        loop {
            let token = session.tokenizer.peek_ignore_padding(session.chunk)?;
            match token.kind() {
                JsTokenKind::Comma => continue,
                JsTokenKind::BracketClose => {
                    range.end = session.source_location(token.end);
                    break;
                }
                _ => match self.parse_expression(session) {
                    Ok((expression_node, _)) => {
                        parameters.push(expression_node)
                    }
                    Err(err) => return Err(err)
                }
            }
        }

        let token = session.tokenizer.peek_token(session.chunk)?;
        match token.kind() {
            JsTokenKind::BracketClose => session.tokenizer.pop_token(session.chunk)?,
            _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Call not closed with bracket")), token.range(session.chunk)))
        };

        return Ok((Expression::Call { identifier, parameters }, None));
    }

    fn parse_declaration(&self, session: &mut JsParserSession, kind: &DeclarationKind) -> Result<(Statement, Option<StatementTerminator>), ParserError> {
        use self::JsTokenKind::*;

        let mut declarations = Vec::new();

        loop {
            let identifier_token = session.tokenizer.peek_ignore_whitespace(session.chunk)?;

            match identifier_token.kind() {
                Identifier(ref name) => {
                    session.tokenizer.pop_ignore_whitespace(session.chunk)?;
                    let mut range = SourceRange::default();
                    range.start = session.source_location(identifier_token.start);

                    let mut trivia = DeclarationTrivia::default();
                    let expression;

                    let next_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
                    match next_token.kind() {
                        Operator(OperatorKind::Assign) => {
                            session.tokenizer.pop_ignore_padding(session.chunk)?;
                            trivia.assign_prefix = next_token.prefix;
                            expression = match self.parse_expression(session) {
                                Ok((expression_node, _)) => {
                                    range.end = expression_node.range().end;
                                    Some(expression_node)
                                }
                                Err(err) => return Err(err),
                            };
                        }
                        _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Declaration is not valid")), next_token.range(session.chunk)))
                    }
                    declarations.push(DeclarationNode::new(name.to_owned(), expression, trivia, range))
                }
                Comma => {
                    session.tokenizer.pop_ignore_whitespace(session.chunk)?;
                }
                Base(BaseTokenKind::Newline) => {
                    break;
                },
                Base(BaseTokenKind::EndOfFile) => {
                    break;
                }
                Semicolon => {
                    break;
                }
                _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Declaration does not end correctly")), identifier_token.range(session.chunk)))
            }
        }

        return Ok((Statement::Declaration { kind: kind.clone(), declarations }, None));
    }

    fn parse_return(&self, session: &mut JsParserSession) -> Result<(Statement, Option<StatementTerminator>), ParserError> {
        let mut expression_option = None;
        let mut terminator_option = None;
        match self.parse_expression(session) {
            Ok((expression_node, terminator)) => {
                expression_option = Some(expression_node);
                terminator_option = terminator;
            }
            Err(_) => {}
        };

        return Ok((Statement::Return { expression: expression_option }, terminator_option));
    }

    fn parse_import(&self, session: &mut JsParserSession) -> Result<(Statement, Option<StatementTerminator>), ParserError> {
        let mut trivia = ImportTrivia::new();

        let declaration_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
        trivia.declaration_prefix = declaration_token.prefix.clone();

        let declaration = match declaration_token.kind() {
            JsTokenKind::Operator(OperatorKind::Multiplication) => ImportDeclaration::All,
            JsTokenKind::Identifier(name) => ImportDeclaration::Single(name.to_owned()),
            _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid import")), declaration_token.range(session.chunk)))
        };

        let mut alias = None;
        let source;

        loop {
            session.tokenizer.pop_ignore_padding(session.chunk)?;
            let next_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
            match next_token.kind() {
                JsTokenKind::Keyword(Keyword::As) => {
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                    if alias.is_some() {
                        return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid import.")), next_token.range(session.chunk)));
                    }
                    trivia.as_prefix = next_token.prefix;
                    let identifier_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
                    match identifier_token.kind() {
                        JsTokenKind::Identifier(ref name) => {
                            trivia.alias_prefix = identifier_token.prefix;
                            alias = Some(name.to_owned());
                        }
                        _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid import")), identifier_token.range(session.chunk)))
                    }
                }
                JsTokenKind::Keyword(Keyword::From) => {
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                    trivia.from_prefix = next_token.prefix;
                    let source_token = session.tokenizer.pop_ignore_padding(session.chunk)?;
                    match source_token.kind() {
                        JsTokenKind::Literal(Literal::String(ref name, ref quote)) => {
                            trivia.source_prefix = source_token.prefix;
                            trivia.quote_kind = quote.clone();
                            source = Some(name.to_owned());
                        }
                        _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid import")), source_token.range(session.chunk)))
                    };
                    break;
                }
                _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid import")), next_token.range(session.chunk)))
            }
        }

        //TODO: Cleanup, should this be here?
        let import = source.unwrap();
        session.requires.insert(import.clone());

        return Ok((Statement::Import { source: import, alias, declaration, trivia }, None));
    }

    fn parse_keyword(&self, session: &mut JsParserSession, keyword: Keyword) -> Result<(Statement, Option<StatementTerminator>), ParserError> {
        use self::Keyword::*;

        return match keyword {
            Return => {
                session.tokenizer.pop_ignore_padding(session.chunk)?;
                self.parse_return(session)
            }
            Declaration(kind) => {
                session.tokenizer.pop_ignore_padding(session.chunk)?;
                self.parse_declaration(session, &kind)
            }
            Import => {
                session.tokenizer.pop_ignore_padding(session.chunk)?;
                self.parse_import(session)
            }
            _ => {
                let token = session.tokenizer.peek_token(session.chunk)?;
                Err(ParserError::new(ParserErrorKind::Syntax(format!("Statement keyword '{:?}' not found", keyword)), token.range(session.chunk)))
            }
        };
    }

    fn parse_statement(&self, session: &mut JsParserSession) -> Result<StatementNode, ParserError> {
        use self::JsTokenKind::*;

        let mut range = SourceRange::default();
        let mut trivia = NodeTrivia::default();
        let mut statement_option: Option<Statement> = None;
        let mut terminator_option: Option<StatementTerminator> = None;

        let statement_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
        range.start = session.source_location(statement_token.start);
        trivia.prefix = statement_token.prefix.clone();
        match statement_token.kind() {
            Keyword(keyword) => {
                let (statement, terminator) = self.parse_keyword(session, keyword)?;
                statement_option = Some(statement);
                terminator_option = terminator;
            }
            Operator(OperatorKind::Division) => {
                let start = session.chunk.index;
                session.tokenizer.pop_ignore_padding(session.chunk)?;
                let next_token = session.tokenizer.peek_token(session.chunk)?;
                session.tokenizer.pop_token(session.chunk)?;
                if next_token.kind() == Operator(OperatorKind::Division) {
                    let mut content = String::new();
                    loop {
                        if session.chunk.is_eof() {
                            break;
                        }
                        let token = session.tokenizer.peek_token(session.chunk)?;
                        session.tokenizer.pop_token(session.chunk)?;
                        if token.kind() == JsTokenKind::Base(BaseTokenKind::Newline) {
                            terminator_option = Some(StatementTerminator::Newline);
                            break;
                        }
                        content += &token.kind().to_string();
                    }
                    statement_option = Some(Statement::Comment { kind: CommentKind::Line, content });
                } else if next_token.kind() == Operator(OperatorKind::Multiplication) {
                    let mut content = String::new();
                    loop {
                        if session.chunk.is_eof() {
                            break;
                        }
                        let token = session.tokenizer.peek_token(session.chunk)?;
                        session.tokenizer.pop_token(session.chunk)?;
                        if token.kind() == JsTokenKind::Base(BaseTokenKind::Newline) {
                            terminator_option = Some(StatementTerminator::Newline);
                            break;
                        } else if token.kind() == JsTokenKind::Operator(OperatorKind::Multiplication) {
                            let next_token = session.tokenizer.peek_token(session.chunk)?;
                            if next_token.kind() == JsTokenKind::Operator(OperatorKind::Division) {
                                session.tokenizer.pop_token(session.chunk)?;
                                terminator_option = Some(StatementTerminator::None);
                                break;
                            } else {
                                content += &token.kind().to_string();
                                content += &next_token.kind().to_string();
                            }
                        } else {
                            content += &token.kind().to_string();
                        }
                    }
                    statement_option = Some(Statement::Comment { kind: CommentKind::Body, content });
                } else {
                    session.chunk.index = start;
                }
            }
            _ => {
                statement_option = None;
            }
        }
        if statement_option.is_none() {
            let (mut expression_node, terminator) = self.parse_expression(session)?;
            expression_node.trivia.prefix = String::new();
            range.start = expression_node.range().start;
            range.end = expression_node.range().end;
            statement_option = Some(Statement::Expression { expression: expression_node });
            terminator_option = terminator;
        }
        if terminator_option.is_none() {
            loop {
                let token = session.tokenizer.peek_token(session.chunk)?;
                range.end = session.source_location(token.end);
                match token.kind() {
                    Base(ref base) => match *base {
                        BaseTokenKind::Whitespace(count) => {
                            for _ in 0..count {
                                trivia.suffix += " ";
                            }
                            session.tokenizer.pop_token(session.chunk)?;
                        }
                        BaseTokenKind::Newline => {
                            session.tokenizer.pop_token(session.chunk)?;
                            let next_proper_token = session.tokenizer.peek_ignore_whitespace(session.chunk)?.kind();

                            if next_proper_token == Base(BaseTokenKind::Newline) || next_proper_token == Semicolon {
                                trivia.suffix += "\n";
                            } else {
                                terminator_option = Some(StatementTerminator::Newline);
                                break;
                            }
                        }
                        BaseTokenKind::EndOfFile => {
                            terminator_option = Some(StatementTerminator::None);
                            break;
                        }
                        _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid end of statement")), token.range(session.chunk)))
                    }
                    Semicolon => {
                        terminator_option = Some(StatementTerminator::Semicolon);
                        session.tokenizer.pop_token(session.chunk)?;
                        break;
                    }
                    BraceClose => {
                        terminator_option = Some(StatementTerminator::Block);
                        session.tokenizer.pop_token(session.chunk)?;
                        break;
                    }
                    _ => {
                        return Err(ParserError::new(ParserErrorKind::Syntax(format!("Statement is terminated with token '{}'", token.kind().to_string())), token.range(session.chunk)));
                    }
                }
            }
        }

        return Ok(StatementNode::new(statement_option.unwrap(), trivia, terminator_option.unwrap(), range));
    }
}
