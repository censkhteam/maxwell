use core::lexer::{ident_table, Tokenizer, TokenizerError, Token};
use core::Chunk;
use core::parser::{SourceRange};
use js::{JsTokenKind, Literal, Keyword, DeclarationKind, OperatorKind};

#[derive(Debug)]
pub struct JsTokenizer {}

impl Tokenizer<JsTokenKind> for JsTokenizer {
    fn pop_token(&self, chunk: &mut Chunk) -> Result<Token<JsTokenKind>, TokenizerError> {
        let start = chunk.index;
        let base_token = self.pop_base_token(chunk)?;
        if base_token.is_some() {
            return Ok(Token::new(JsTokenKind::Base(base_token.unwrap()), start, chunk.index));
        }

        let char = chunk.peek_char();
        let kind = match char {
            '=' => {
                chunk.bump_char();
                let operator = match chunk.peek_char() {
                    '=' => {
                        chunk.bump_char();
                        match chunk.peek_char() {
                            '=' => {
                                chunk.bump_char();
                                OperatorKind::StrictEquality
                            }
                            _ => OperatorKind::Equality
                        }
                    }
                    '>' => {
                        chunk.bump_char();
                        return Ok(Token::new(JsTokenKind::FatArrow, start, chunk.index));
                    }
                    _ => OperatorKind::Assign
                };
                JsTokenKind::Operator(operator)
            }
            '+' => {
                chunk.bump_char();
                if chunk.peek_char() == '=' {
                    chunk.bump_char();
                    JsTokenKind::Operator(OperatorKind::AddAssign)
                } else {
                    JsTokenKind::Operator(OperatorKind::Addition)
                }
            }
            '*' => {
                chunk.bump_char();
                if chunk.peek_char() == '=' {
                    chunk.bump_char();
                    JsTokenKind::Operator(OperatorKind::MultiplyAssign)
                } else {
                    JsTokenKind::Operator(OperatorKind::Multiplication)
                }
            }
            '/' => {
                chunk.bump_char();
                if chunk.peek_char() == '=' {
                    chunk.bump_char();
                    JsTokenKind::Operator(OperatorKind::DivideAssign)
                } else {
                    JsTokenKind::Operator(OperatorKind::Division)
                }
            }
            '(' => {
                chunk.bump_char();
                JsTokenKind::BracketOpen
            }

            ')' => {
                chunk.bump_char();
                JsTokenKind::BracketClose
            }
            '{' => {
                chunk.bump_char();
                JsTokenKind::BraceOpen
            }
            '}' => {
                chunk.bump_char();
                JsTokenKind::BraceClose
            }
            ';' => {
                chunk.bump_char();
                JsTokenKind::Semicolon
            }
            ':' => {
                chunk.bump_char();
                JsTokenKind::Colon
            }
            '.' => {
                let start = chunk.index;
                chunk.bump_char();
                match chunk.peek_char() {
                    '0' ... '9' => JsTokenKind::Literal(Literal::Number(self.read_number(chunk, start).to_owned())),
                    '.' => {
                        unimplemented!();
                    }
                    _ => JsTokenKind::Operator(OperatorKind::Accessor)
                }
            }
            ',' => {
                chunk.bump_char();
                JsTokenKind::Comma
            }
            '0' ... '9' => {
                let start = chunk.index;
                JsTokenKind::Literal(Literal::Number(self.read_number(chunk, start).to_owned()))
            }
            '\'' => {
                let (value, quote) = self.read_quote_until(chunk, '\'');
                JsTokenKind::Literal(Literal::String(value, quote))
            }
            '"' => {
                let (value, quote) = self.read_quote_until(chunk, '"');
                JsTokenKind::Literal(Literal::String(value, quote))
            }
            _ => {
                if ident_table::is_ident(char) {
                    let label = chunk.consume_label();
                    match label {
                        "if" => JsTokenKind::Keyword(Keyword::If),
                        "function" => JsTokenKind::Keyword(Keyword::Function),
                        "return" => JsTokenKind::Keyword(Keyword::Return),
                        "while" => JsTokenKind::Keyword(Keyword::While),
                        "switch" => JsTokenKind::Keyword(Keyword::Switch),

                        "finally" => JsTokenKind::Keyword(Keyword::Function),
                        "with" => JsTokenKind::Keyword(Keyword::Return),
                        "in" => JsTokenKind::Keyword(Keyword::In),
                        "break" => JsTokenKind::Keyword(Keyword::Break),
                        "do" => JsTokenKind::Keyword(Keyword::Do),

                        "class" => JsTokenKind::Keyword(Keyword::Class),
                        "extends" => JsTokenKind::Keyword(Keyword::Extends),

                        "async" => JsTokenKind::Keyword(Keyword::Async),
                        "await" => JsTokenKind::Keyword(Keyword::Await),

                        "import" => JsTokenKind::Keyword(Keyword::Import),
                        "export" => JsTokenKind::Keyword(Keyword::Export),

                        "from" => JsTokenKind::Keyword(Keyword::From),
                        "try" => JsTokenKind::Keyword(Keyword::Try),
                        "catch" => JsTokenKind::Keyword(Keyword::Catch),

                        "of" => JsTokenKind::Keyword(Keyword::Of),
                        "yield" => JsTokenKind::Keyword(Keyword::Yield),

                        "let" => JsTokenKind::Keyword(Keyword::Declaration(DeclarationKind::Let)),
                        "var" => JsTokenKind::Keyword(Keyword::Declaration(DeclarationKind::Var)),
                        "const" => JsTokenKind::Keyword(Keyword::Declaration(DeclarationKind::Const)),
                        "null" => JsTokenKind::Literal(Literal::Null),
                        "undefined" => JsTokenKind::Literal(Literal::Undefined),
                        "true" => JsTokenKind::Literal(Literal::Boolean(true)),
                        "false" => JsTokenKind::Literal(Literal::Boolean(false)),
                        value => JsTokenKind::Identifier(value.to_owned())
                    }
                } else {
                    return Err(TokenizerError::new(format!("Invalid token {}", char), SourceRange::new(chunk.source_location(start), chunk.current_source_location())));
                }
            }
        };
        return Ok(Token::new(kind, start, chunk.index));
    }
}

impl JsTokenizer {
    pub fn new() -> Self {
        return JsTokenizer {};
    }

    fn read_number<'a>(&self, chunk: &'a mut Chunk, start: usize) -> &'a str {
        while !chunk.is_eof() {
            match chunk.peek_char() {
                '0' ... '9' => {
                    chunk.bump_char();
                }
                '.' => {
                    chunk.bump_char();
                }
                _ => break
            }
        }

        return chunk.slice(start, chunk.index);
    }
}