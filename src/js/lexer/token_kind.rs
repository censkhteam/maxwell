use super::{Literal, Keyword, OperatorKind};
use core::lexer::{TokenKind, BaseTokenKind};
use self::JsTokenKind::*;

#[derive(Debug, PartialEq, Clone)]
pub enum JsTokenKind {
    Base(BaseTokenKind),
    Semicolon,
    Colon,
    Comma,
    FatArrow,
    BracketOpen,
    BracketClose,
    BraceOpen,
    BraceClose,
    Operator(OperatorKind),
    Identifier(String),
    Literal(Literal),
    Keyword(Keyword),
}

impl TokenKind for JsTokenKind {
    fn is_new_line(&self) -> bool {
        return match *self {
            Base(BaseTokenKind::Newline) => true,
            _ => false
        };
    }

    fn get_whitespace(&self) -> u8 {
        return match *self {
            Base(BaseTokenKind::Whitespace(count)) => count,
            _ => 0
        };
    }

    fn to_string(&self) -> String {
        return match *self {
            Base(ref base) => base.to_string(),
            BraceClose => String::from("}"),
            BraceOpen => String::from("{"),
            FatArrow => String::from("=>"),
            BracketClose => String::from(")"),
            BracketOpen => String::from("("),
            Semicolon => String::from(";"),
            Colon => String::from(":"),
            Comma => String::from(","),
            Operator(ref operator) => operator.to_string(),
            Identifier(ref string) => string.to_string(),
            Keyword(ref keyword) => keyword.to_string(),
            Literal(ref literal) => literal.to_string(),
        };
    }
}