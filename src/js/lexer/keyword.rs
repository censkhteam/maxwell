use js::DeclarationKind;
use std::fmt;

#[derive(Debug, PartialEq, Clone)]
pub enum Keyword {
    Function,
    Default,
    While,
    This,
    Finally,
    With,
    Switch,
    Yield,
    Break,
    Do,
    In,
    Of,
    Class,
    Extends,
    Return,
    Import,
    Export,
    Try,
    As,
    Catch,
    If,
    From,
    Async,
    Await,
    Declaration(DeclarationKind),
}

impl fmt::Display for Keyword {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Keyword::*;

        let string = match *self {
            From => String::from("from"),
            Function => String::from("function"),
            Break => String::from("break"),
            Catch => String::from("catch"),
            If => String::from("if"),
            Class => String::from("class"),
            Default => String::from("default"),
            Yield => String::from("yield"),
            Async => String::from("async"),
            While => String::from("while"),
            As => String::from("as"),
            This => String::from("this"),
            Finally => String::from("finally"),
            Switch => String::from("switch"),
            Return => String::from("return"),
            Extends => String::from("extends"),
            Try => String::from("try"),
            Await => String::from("await"),
            Do => String::from("do"),
            Export => String::from("export"),
            Import => String::from("import"),
            With => String::from("with"),
            In => String::from("in"),
            Of => String::from("of"),

            Declaration(ref declaration) => declaration.to_string(),
        };

        write!(f, "{}", string)
    }
}