use super::super::{StatementTerminator, Expression};
use core::transform::{PluginPass, Plugin};

use std::error::Error;

#[derive(Debug)]
pub struct PrettifyPlugin {}

impl Plugin for PrettifyPlugin {
    fn handle(&self, pass: &mut PluginPass) -> Result<(), Box<Error>> {
        use self::PluginPass::*;
        use self::Expression::*;

        match pass {
            &mut StatementNode(ref mut statement_node) => {
                statement_node.trivia.prefix = String::from("\n");
                if statement_node.terminator == StatementTerminator::Newline {
                    statement_node.terminator = StatementTerminator::Semicolon;
                }
                Ok(())
            }
            &mut ExpressionNode(ref mut expression_node) => {
                match expression_node.expression {
                    Function { ref mut trivia, .. } => {
                        //TODO: trivia.cleanup()
                        trivia.identifier_gap = String::from(" ");
                        trivia.parameters_gap = String::from("");
                        trivia.body_gap = String::from(" ");
                        trivia.body_suffix = String::from("\n");
                        trivia.parameters_padding = String::from("");

                        Ok(())
                    }
                    _ => Ok(())
                }
            }
        }
    }

    fn get_name(&self) -> &str {
        return "internal.prettify";
    }
}