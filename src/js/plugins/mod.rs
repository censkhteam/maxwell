mod minify_plugin;
mod prettify_plugin;

pub use self::prettify_plugin::*;
pub use self::minify_plugin::*;