use js::{StatementTerminator, Statement, Expression, CommentKind};

use core::transform::{Plugin, PluginPass};

use std::error::Error;

#[derive(Debug)]
pub struct MinifyPlugin {}

impl Plugin for MinifyPlugin {
    fn handle(&self, pass: &mut PluginPass) -> Result<(), Box<Error>> {
        use self::PluginPass::*;
        use self::Expression::*;

        match *pass {
            StatementNode(ref mut statement_node) => {
                statement_node.trivia.prefix = String::from("");
                statement_node.trivia.suffix = String::from("");
                if statement_node.terminator == StatementTerminator::Newline {
                    statement_node.terminator = StatementTerminator::Semicolon;
                }

                match statement_node.statement {
                    Statement::Comment { ref mut kind, ref content } => {
                        *kind = CommentKind::Body;
                        statement_node.terminator = StatementTerminator::None;
                    }
                    _ => {}
                }

                Ok(())
            }
            ExpressionNode(ref mut expression_node) => {
                expression_node.trivia.prefix = String::from("");
                expression_node.trivia.suffix = String::from("");
                match expression_node.expression {
                    Function { ref mut trivia, .. } => {
                        //TODO: trivia.cleanup()
                        trivia.identifier_gap = String::from(" ");
                        trivia.parameters_gap = String::from("");
                        trivia.body_gap = String::from("");
                        trivia.body_suffix = String::from("");
                        trivia.parameters_padding = String::from("");

                        Ok(())
                    },
                    Operation { ref mut left_hand, ref mut right_hand, ..} => {
                        Ok(())
                    }
                    _ => Ok(())
                }
            }
        }
    }

    fn get_name(&self) -> &str {
        return "internal.minify";
    }
}