use core::parser::Token;
use js::JsToken;

#[derive(Debug, PartialEq, Clone)]
pub enum TsToken {
    Js(JsToken)
}

impl Token for TsToken {
    fn to_string(&self) -> String {
        use self::TsToken::*;

        return match *self {
            Js(ref js) => js.to_string(),
        };
    }
}