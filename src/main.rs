extern crate term_painter;

use term_painter::ToStyle;
use term_painter::Color::*;

extern crate maxwell;
extern crate clap;

mod core;
mod js;
mod ts;
mod html;
mod css;

use core::{Compiler, JsonConfigLoader, CompilerConfig};
use core::output::FileSystemOutput;

use clap::{Arg, App};

fn main() {
    let matches = App::new("Maxwell")
        .version("0.0.1")
        .author("James W. <09jwater@gmail.com>")
        .about("A Javascript baker.")
        .arg(Arg::with_name("config")
            .short("c")
            .long("config")
            .help("Points to a custom config file.")
            .value_name("CONFIG")
            .takes_value(true))
        .arg(Arg::with_name("watch")
            .short("w")
            .long("watch")
            .help("Watch for changes."))
        .get_matches();

    let mut config = CompilerConfig::default();
    let config_loader = JsonConfigLoader {};
    match config_loader.load(matches.value_of("config").unwrap_or("./maxwell.json").to_owned(), &mut config) {
        Ok(_) => {}
        Err(err) => {
            eprintln!("{}", BrightRed.paint(format!("Compiler error: \n{}", err.to_string())));
            return;
        }
    }
    config.output = Some(FileSystemOutput::new(config.src_dir, config.out_dir));
    config.watch = matches.is_present("watch");

    if config.watch {
        compiler.watch(config);
    } else {
        match compiler.run(config) {
            Ok(result) => result.print(),
            Err(err) => {
                eprintln!("{}", BrightRed.paint(format!("Compiler error: \n{}", err.to_string())));
            }
        }
    }
}

