use core::lexer::{Tokenizer, TokenizerError, Token};
use core::Chunk;

use html::HtmlTokenKind;

#[derive(Debug)]
pub struct HtmlTokenizer {}

impl Tokenizer<HtmlTokenKind> for HtmlTokenizer {
    fn pop_token(&self, chunk: &mut Chunk) -> Result<Token<HtmlTokenKind>, TokenizerError> {
        let start = chunk.index;
        let base_token = self.pop_base_token(chunk)?;
        if base_token.is_some() {
            return Ok(Token::new(HtmlTokenKind::Base(base_token.unwrap()), start, chunk.index));
        }

        let char = chunk.peek_char();

        let kind = match char {
            '\'' => {
                let (value, quote) = self.read_quote_until(chunk, '\'');
                HtmlTokenKind::Value(value, quote)
            }
            '"' => {
                let (value, quote) = self.read_quote_until(chunk, '"');
                HtmlTokenKind::Value(value, quote)
            }
            '!' => {
                chunk.bump_char();
                HtmlTokenKind::Exclamation
            }
            '=' => {
                chunk.bump_char();
                HtmlTokenKind::Equals
            }
            '<' => {
                chunk.bump_char();
                HtmlTokenKind::TagOpen
            }
            '>' => {
                chunk.bump_char();
                HtmlTokenKind::TagClose
            }
            '/' => {
                chunk.bump_char();
                HtmlTokenKind::Slash
            }
            _ => {
                let label = chunk.consume_label();
                HtmlTokenKind::Identifier(label.to_owned())
            }
        };

        return Ok(Token::new(kind, start, chunk.index));
    }
}

impl HtmlTokenizer {
    pub fn new() -> Self {
        return HtmlTokenizer {};
    }
}