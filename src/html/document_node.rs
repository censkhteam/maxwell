use core::transform::PluginPass;
use core::parser::{Node, NodeList, SourceRange};
use core::output::CompilerOutput;

use html::TagChild;

#[derive(Debug, Clone, PartialEq)]
pub struct DocumentNode {
    pub content: Vec<TagChild>,
    range: SourceRange
}

impl DocumentNode {
    pub fn new(content: Vec<TagChild>) -> Self {
        return DocumentNode {
            range: SourceRange::new(
                content.first().unwrap().node().range().start,
                content.last().unwrap().node().range().end,
            ),
            content,
        };
    }
}

impl Node for DocumentNode {
    fn generate(&self, output: &mut Box<CompilerOutput>) {
        for child in &self.content {
            child.generate(output);
        }
    }

    fn children<'a>(&'a mut self) -> Option<NodeList<'a>> {
        let mut children: NodeList = Vec::new();

        for child in &mut self.content {
            children.push(child.node_mut());
        }

        return Some(children);
    }

    fn plugin_passes(&mut self) -> Vec<PluginPass> {
        return vec![];
    }
    fn range<'a>(&'a self) -> &'a SourceRange {
        return &self.range;
    }
}