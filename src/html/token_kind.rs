use core::lexer::{TokenKind, BaseTokenKind};
use core::parser::QuoteKind;
use self::HtmlTokenKind::*;

#[derive(Debug, PartialEq, Clone)]
pub enum HtmlTokenKind {
    Base(BaseTokenKind),
    Identifier(String),
    TagOpen,
    TagClose,
    Equals,
    Exclamation,
    Value(String, QuoteKind),
    Slash
}

impl TokenKind for HtmlTokenKind {
    fn is_new_line(&self) -> bool {
        return match *self {
            Base(BaseTokenKind::Newline) => true,
            _ => false
        };
    }

    fn get_whitespace(&self) -> u8 {
        return match *self {
            Base(BaseTokenKind::Whitespace(count)) => count,
            _ => 0
        };
    }

    fn to_string(&self) -> String {
        return match *self {
            Base(ref base) => base.to_string(),
            Identifier(ref name) => name.to_string(),
            TagClose => String::from(">"),
            TagOpen => String::from("<"),
            Slash => String::from("/"),
            Exclamation => String::from("!"),
            Equals => String::from("="),
            Value(ref value, ref quote) => format!("{}{}{}", quote.to_string(), value, quote.to_string())
        };
    }
}