use core::parser::{Node, SourceRange, NodeList};
use core::transform::PluginPass;
use core::output::CompilerOutput;

use html::{TagChild, Attribute};

#[derive(Debug, Default, PartialEq, Clone)]
pub struct TagTrivia {
    pub prefix: String,
    pub suffix: String,
    pub inner_suffix: String,
    pub tag_close_prefix: String,
    pub tag_open_suffix: String,
    pub attributes_suffix: String,
}

#[derive(Debug, PartialEq, Clone)]
pub enum TagKind {
    Normal,
    AutoClose,
    BeginClosed,
    NoClose,
}

#[derive(Debug, PartialEq, Clone)]
pub struct TagNode {
    pub trivia: TagTrivia,
    pub name: String,
    pub attributes: Vec<Attribute>,
    pub content: Vec<TagChild>,
    range: SourceRange,
    pub kind: TagKind,
}

impl TagNode {
    pub fn new(name: String, kind: TagKind, content: Vec<TagChild>, attributes: Vec<Attribute>, trivia: TagTrivia, range: SourceRange) -> Self {
        return TagNode {
            name,
            kind,
            trivia,
            content,
            attributes,
            range,
        };
    }
}

impl Node for TagNode {
    fn generate(&self, output: &mut Box<CompilerOutput>) {
        let (start_slash, end_slash) = match self.kind {
            TagKind::Normal | TagKind::NoClose => ("", ""),
            TagKind::AutoClose => ("", "/"),
            TagKind::BeginClosed => ("/", ""),
        };

        output.write_string(&self.trivia.prefix);
        output.write_string(start_slash);
        output.write_string("<");
        output.write_string(&self.name);

        for attribute in &self.attributes {
            attribute.generate(output);
        }

        output.write_string(&self.trivia.attributes_suffix);
        output.write_string(end_slash);
        output.write_string(&self.trivia.tag_open_suffix);
        output.write_string(">");

        for tag in &self.content {
            tag.generate(output);
        }
        if self.kind == TagKind::Normal {
            output.write_string(&self.trivia.inner_suffix);
            output.write_string("</");
            output.write_string(&self.name);
            output.write_string(&self.trivia.tag_close_prefix);
            output.write_string(">");
        }
        output.write_string(&self.trivia.suffix);
    }

    fn plugin_passes(&mut self) -> Vec<PluginPass> {
        return vec![];
    }

    fn children<'a>(&'a mut self) -> Option<NodeList<'a>> {
        let mut children: NodeList = Vec::new();

        for child in &mut self.content {
            children.push(child.node_mut());
        }

        return Some(children);
    }
    fn range<'a>(&'a self) -> &'a SourceRange {
        return &self.range;
    }
}