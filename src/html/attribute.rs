use core::parser::QuoteKind;
use core::output::CompilerOutput;

#[derive(Debug, Clone, Default, PartialEq)]
pub struct AttributeTrivia {
    pub name_prefix: String,
    pub name_suffix: String,
    pub value_prefix: String,
    pub value_suffix: String,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Attribute {
    pub quote_kind: QuoteKind,
    pub trivia: AttributeTrivia,
    pub name: String,
    pub value: Option<String>,
}

impl Attribute {
    pub fn new(name: String, quote_kind: QuoteKind, value: Option<String>, trivia: AttributeTrivia) -> Self {
        return Attribute {
            trivia,
            name,
            value,
            quote_kind
        };
    }

    pub fn generate(&self, output: &mut Box<CompilerOutput>) {
        output.write_string(&self.trivia.name_prefix);
        output.write_string(&self.name);
        output.write_string(&self.trivia.name_suffix);

        match self.value {
            Some(ref value) => {
                output.write_string("=");
                output.write_string(&self.trivia.value_prefix);
                output.write_string(&self.quote_kind.to_string());
                output.write_string(value);
                output.write_string(&self.quote_kind.to_string());
            },
            None => {
                output.write_string(&self.trivia.value_prefix);
            },
        };

        output.write_string(&self.trivia.value_suffix);

    }
}