use core::parser::{Node, NodeTrivia, SourceRange, NodeList};
use core::transform::PluginPass;
use core::output::CompilerOutput;

#[derive(Debug, PartialEq, Clone)]
pub struct TextNode {
    pub trivia: NodeTrivia,
    pub content: String,
    range: SourceRange,
}

impl TextNode {
    pub fn new(content: String, trivia: NodeTrivia, range: SourceRange) -> Self {
        return TextNode {
            content,
            trivia,
            range,
        };
    }
}

impl Node for TextNode {
    fn generate(&self, output: &mut Box<CompilerOutput>) {
        output.write_string(&self.trivia.prefix);
        output.write_string(&self.content);
        output.write_string(&self.trivia.suffix);
    }

    fn plugin_passes(&mut self) -> Vec<PluginPass> {
        return vec![];
    }

    fn children<'a>(&'a mut self) -> Option<NodeList<'a>> {
        return None;
    }

    fn range<'a>(&'a self) -> &'a SourceRange {
        return &self.range;
    }
}