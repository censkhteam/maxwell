use core::parser::*;
use core::Chunk;
use core::lexer::{BaseTokenKind, TokenKind};

use html::*;
use html::HtmlTokenKind::*;

use std::time::Instant;

type HtmlParserSession<'a> = ParserSession<'a, HtmlTokenKind>;

#[derive(Debug)]
pub struct HtmlParser {}

struct TagAttributesResult {
    attributes: Vec<Attribute>,
    suffix: String,
    tag_kind: Option<TagKind>,
}

enum TagParseResult {
    Child(TagChild),
    Close(String),
}

impl Parser for HtmlParser {
    fn parse(&self, chunk: &mut Chunk) -> Result<ParserResult, ParserError> {
        use self::HtmlTokenKind::*;

        let start = Instant::now();
        let mut session = ParserSession::new(chunk, Box::new(HtmlTokenizer::new()));
        session.chunk.index = 0;

        let mut contents = Vec::new();

        loop {
            let token = session.tokenizer.peek_ignore_padding(session.chunk)?;
            if token.kind() == Base(BaseTokenKind::EndOfFile) {
                break;
            }
            match self.parse_node(&mut session) {
                Ok(tag_node) => {
                    match tag_node {
                        Some(tag) => contents.push(tag),
                        None => {}
                    }
                }
                Err(err) => {
                    session.tokenizer.pop_token(session.chunk)?;
                    session.log.add_message(session.chunk.id(), Box::new(err));
                }
            }
        }

        session.chunk.syntax_tree = Some(SyntaxTree::new(Box::new(DocumentNode::new(contents))));
        return Ok(ParserResult::new(session, start.elapsed()));
    }

    fn query_extension(&self, string: &str) -> bool {
        return string.eq("html") || string.eq("htm");
    }

    fn name(&self) -> &'static str {
        return "html";
    }
}


impl HtmlParser {
    pub fn new() -> Self {
        return HtmlParser {};
    }

    fn parse_text_until(&self, session: &mut HtmlParserSession, point: usize) -> Result<TagChild, ParserError> {
        let mut range = SourceRange::default();
        let trivia = NodeTrivia::default();
        let mut content = String::new();

        let mut token = session.tokenizer.pop_token(session.chunk)?;
        range.start = session.source_location(token.start);

        content += &token.prefix;
        content += &token.kind().to_string();
        loop {
            if session.chunk.index == point {
                break;
            }
            token = session.tokenizer.peek_token(session.chunk)?;
            session.tokenizer.pop_token(session.chunk)?;
            content += &token.prefix;
            content += &token.kind().to_string();
        }
        range.end = session.source_location(token.end);
        return Ok(TagChild::Text(TextNode::new(content, trivia, range)));
    }

    fn parse_text(&self, session: &mut HtmlParserSession) -> Result<TagChild, ParserError> {
        let mut range = SourceRange::default();
        let trivia = NodeTrivia::default();
        let mut content = String::new();

        let mut token = session.tokenizer.pop_token(session.chunk)?;
        range.start = session.source_location(token.start);

        content += &token.prefix;
        content += &token.kind().to_string();
        loop {
            token = session.tokenizer.peek_token(session.chunk)?;
            match token.kind() {
                TagOpen => break,
                Base(BaseTokenKind::EndOfFile) => break,
                token_kind => {
                    session.tokenizer.pop_token(session.chunk)?;
                    content += &token.prefix;
                    content += &token_kind.to_string();
                }
            }
        }
        range.end = session.source_location(token.end);
        return Ok(TagChild::Text(TextNode::new(content, trivia, range)));
    }

    fn parse_tag_attributes(&self, session: &mut HtmlParserSession, tag_name: &String, range: &mut SourceRange) -> Result<TagAttributesResult, ParserError> {
        let mut attributes = Vec::new();
        let mut suffix = String::new();
        let mut tag_kind = None;
        loop {
            let token = session.tokenizer.peek_ignore_padding(session.chunk)?;
            match token.kind() {
                TagClose => {
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                    suffix = token.prefix;
                    range.end = session.source_location(token.end);
                    break;
                }
                Slash => {
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                    let next_token = session.tokenizer.peek_token(session.chunk)?;
                    if next_token.kind() == TagClose {
                        suffix = token.prefix;
                        session.tokenizer.pop_token(session.chunk)?;
                        tag_kind = Some(TagKind::AutoClose);
                        break;
                    } else {
                        return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid autoclose tag, next token was {}", next_token.kind().to_string())), next_token.range(session.chunk)));
                    }
                }
                TagOpen => {
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                    return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid tag token {}", token.kind().to_string())), token.range(session.chunk)));
                }
                Identifier(ref name) => {
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                    let mut trivia = AttributeTrivia::default();
                    trivia.name_prefix = token.prefix;
                    let equals_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
                    let (value, quote_kind) = match equals_token.kind() {
                        Equals => {
                            session.tokenizer.pop_ignore_padding(session.chunk)?;
                            trivia.name_suffix = equals_token.prefix;
                            let value_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
                            match value_token.kind() {
                                Value(ref value, ref quote_kind) => {
                                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                                    //TODO: Move
                                    if tag_name == "script" && name == "src" {
                                        session.requires.insert(value.clone());
                                    }
                                    (Some(value.clone()), quote_kind.clone())
                                }
                                _ => (None, QuoteKind::SpeechMark)
                            }
                        }
                        _ => (None, QuoteKind::SpeechMark)
                    };
                    attributes.push(Attribute::new(name.to_owned(), quote_kind, value, trivia));
                }
                Base(BaseTokenKind::EndOfFile) => {
                    range.end = session.source_location(token.end);
                    break;
                }
                _ => {
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                }
            }
        }
        return Ok(TagAttributesResult { attributes, suffix, tag_kind });
    }

    fn parse_tag(&self, session: &mut HtmlParserSession) -> Result<TagChild, ParserError> {
        let mut range = SourceRange::default();
        let token = session.tokenizer.pop_ignore_padding(session.chunk)?;
        range.start = session.source_location(token.start);
        let mut trivia = TagTrivia::default();
        let mut kind = TagKind::Normal;
        trivia.prefix = token.prefix;
        let name = loop {
            let identifier_token = session.tokenizer.peek_token(session.chunk)?;
            match identifier_token.kind() {
                Slash => {
                    session.tokenizer.pop_token(session.chunk)?;
                    kind = TagKind::BeginClosed;
                }
                Identifier(name) => {
                    session.tokenizer.pop_token(session.chunk)?;
                    break name
                }
                _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid token for tag identifier")), identifier_token.range(session.chunk)))
            };
        };
        let attributes_result = self.parse_tag_attributes(session, &name, &mut range)?;
        if attributes_result.tag_kind.is_some() {
            kind = attributes_result.tag_kind.unwrap();
        }
        trivia.attributes_suffix = attributes_result.suffix;
        let mut children: Vec<TagChild> = Vec::new();
        if kind == TagKind::Normal {
            loop {
                let next_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
                let start = session.chunk.index;
                if next_token.kind() == Base(BaseTokenKind::EndOfFile) {
                    kind = TagKind::NoClose;
                    range.end = session.source_location(next_token.end);
                    break;
                }
                if next_token.kind() == TagOpen {
                    session.tokenizer.pop_ignore_padding(session.chunk)?;
                    let tag_token = session.tokenizer.peek_token(session.chunk)?;
                    if tag_token.kind() == Slash {
                        trivia.inner_suffix = next_token.prefix;
                        session.tokenizer.pop_token(session.chunk)?;
                        let token = session.tokenizer.pop_token(session.chunk)?;
                        match token.kind() {
                            Identifier(_) => {}
                            _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Invalid token in closed tag")), token.range(session.chunk)))
                        }
                        let close_token = session.tokenizer.peek_ignore_padding(session.chunk)?;
                        match close_token.kind() {
                            TagClose => {
                                trivia.tag_close_prefix = close_token.prefix;
                                range.end = session.source_location(close_token.end);
                                session.tokenizer.pop_ignore_padding(session.chunk)?;
                            }
                            _ => return Err(ParserError::new(ParserErrorKind::Syntax(format!("Tag closes require idents after slash")), close_token.range(session.chunk)))
                        }
                        break;
                    }
                }

                session.chunk.index = start;
                match self.parse_node(session) {
                    Ok(child) => {
                        if let Some(child) = child {
                            children.push(child)
                        }
                    }
                    Err(err) => return Err(err)
                }
            }
        }
        return Ok(TagChild::Tag(TagNode::new(name.to_owned(), kind, children, attributes_result.attributes, trivia, range)));
    }

    fn parse_node(&self, session: &mut HtmlParserSession) -> Result<Option<TagChild>, ParserError> {
        let token = session.tokenizer.peek_ignore_padding(session.chunk)?;

        let start = session.chunk.index;
        let child = match token.kind() {
            TagOpen => {
                match self.parse_tag(session) {
                    Ok(text) => text,
                    Err(err) => {
                        session.log.add_message(session.chunk.id(), Box::new(err));
                        let error_point = session.chunk.index;
                        session.chunk.index = start;
                        self.parse_text_until(session, error_point)?
                    }
                }
            }
            Base(BaseTokenKind::EndOfFile) => {
                session.tokenizer.pop_ignore_padding(session.chunk)?;
                session.chunk.index -= 1;
                return Ok(None);
            }
            _ => self.parse_text(session)?
        };
        return Ok(Some(child));
    }
}
