use core::parser::Node;
use core::output::CompilerOutput;

use html::{TextNode, TagNode};


#[derive(Debug, PartialEq, Clone)]
pub enum TagChild {
    Text(TextNode),
    Tag(TagNode)
}

impl TagChild {
    pub fn node_mut<'a>(&'a mut self) -> &'a mut Node {
        return match *self {
            TagChild::Text(ref mut node) => node,
            TagChild::Tag(ref mut node) => node,
        };
    }

    pub fn node<'a>(&'a self) -> &'a Node {
        return match *self {
            TagChild::Text(ref node) => node,
            TagChild::Tag(ref node) => node,
        };
    }

    pub fn generate(&self, output : &mut Box<CompilerOutput>) {
        return self.node().generate(output);
    }
}