extern crate json;

use super::{Chunk, ChunkLocation, CompilerConfig};

use std::io::prelude::*;
use std::fs::File;

use std::path::PathBuf;

use std::env;
use std::ops::Index;

//TODO: Cleanup move env vars away from this struct.

#[derive(Debug)]
pub enum LoadError {
    FileNotFound(String),
    FileContentsInvalid,
    InvalidConfigOption(String, String),
    MissingConfigOption(String),
    JsonInvalid,
}

#[derive(Debug, Clone)]
pub struct JsonConfigLoader {}

impl ToString for LoadError {
    fn to_string(&self) -> String {
        use self::LoadError::*;

        return match *self {
            MissingConfigOption(ref string) => format!("Config option '{}' is missing", string),
            FileNotFound(ref path) => format!("File not found at path {}", path),
            JsonInvalid => "Invalid json contents".to_owned(),
            FileContentsInvalid => "File contents are invalid".to_owned(),
            InvalidConfigOption(ref option, ref reason) => format!("Invalid config option '{}': {}", option, reason),
        };
    }
}

impl JsonConfigLoader {
    pub fn load(&self, location: String, config: &mut CompilerConfig) -> Result<(), LoadError> {
        let mut dir_path = env::current_dir().unwrap().to_path_buf();
        dir_path.push(PathBuf::from(location));

        match dir_path.canonicalize() {
            Ok(path) => dir_path = path,
            Err(_) => {
                return Err(LoadError::FileNotFound(dir_path.to_str().unwrap().to_owned()));
            }
        }

        let mut config_name = String::from("maxwell.json");
        if !dir_path.is_dir() {
            config_name = dir_path.file_name().unwrap().to_str().unwrap().to_string();
            dir_path = dir_path.parent().unwrap().to_path_buf();
        }

        config.working_dir = dir_path;

        let config_path = config.working_dir.join(PathBuf::from(config_name)).canonicalize().unwrap();

        match self.read_json(config_path) {
            Ok(node) => {
                match self.parse_json(config, node) {
                    Ok(_) => {}
                    Err(err) => return Err(err),
                };
            }
            Err(err) => return Err(err)
        }
        return Ok(());
    }

    fn parse_json(&self, config: &mut CompilerConfig, node: json::JsonValue) -> Result<(), LoadError> {
        let out_element = node.index("out");
        if !node.has_key("out") {
            return Err(LoadError::MissingConfigOption(String::from("out")));
        }
        let mut out = out_element.as_str().unwrap();
        if out[..2] == String::from("./") {
            out = &out[2..];
        }
        let mut out_dir = config.working_dir.clone();
        for item in out.split("/") {
            out_dir.push(item);
        }

        if !out_dir.is_absolute() {
            return Err(LoadError::InvalidConfigOption(String::from("out"), String::from("Out directory is not valid")));
        }

        config.out_dir = out_dir;

        if !node.has_key("src") {
            return Err(LoadError::MissingConfigOption(String::from("src")));
        }
        let src_node = node.index("src");
        if !src_node.has_key("base") {
            return Err(LoadError::MissingConfigOption(String::from("src.base")));
        }

        let base_dir_element = src_node.index("base");
        let mut base_dir = config.working_dir.clone();
        if base_dir_element.as_str().unwrap() == "." {} else {
            base_dir.push(PathBuf::from(base_dir_element.as_str().unwrap()));
        }
        config.src_dir = base_dir;
        if !config.src_dir.is_absolute() {
            return Err(LoadError::InvalidConfigOption(String::from("src.base"), String::from("Cannot find base directory")));
        }

        if !src_node.has_key("entry") {
            return Err(LoadError::MissingConfigOption(String::from("src.entry")));
        }
        let mut entry_strings = Vec::new();
        let entry_element = src_node.index("entry");
        if entry_element.is_string() {
            entry_strings.push(entry_element.as_str().unwrap().to_owned());
        }
        if entry_element.is_array() {
            for i in 0..entry_element.len() {
                entry_strings.push(entry_element.index(i).as_str().unwrap().to_owned());
            }
        }

        for entry_string in &entry_strings {
            let mut entry = entry_string.to_owned();
            entry = entry.replace("/", "\\");
            let location = ChunkLocation::absolute(config.src_dir.join(PathBuf::from(entry)));
            if location.is_err() {
                return Err(LoadError::FileNotFound(entry_string.to_owned()));
            }
            config.entries.push(location.unwrap());
        }
        return Ok(());
    }

    fn read_json(&self, config_path: PathBuf) -> Result<json::JsonValue, LoadError> {
        let config_file_result = File::open(&config_path);
        if config_file_result.is_err() {
            return Err(LoadError::FileNotFound(config_path.to_str().unwrap().to_owned()));
        }
        let mut contents = String::new();
        if config_file_result.unwrap().read_to_string(&mut contents).is_err() {
            return Err(LoadError::FileContentsInvalid);
        }

        return match json::parse(&contents) {
            Ok(node) => Ok(node),
            Err(_) => Err(LoadError::JsonInvalid)
        };
    }
}