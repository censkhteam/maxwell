use core::{CompilerErrorKind, CompilerError};

use std::error::Error;
use std::fmt;
use std::io;

#[derive(Debug)]
pub enum ChunkErrorKind {
    TooLarge,
    NotFound,
    UnknownIo,
    UnsupportedFormat
}

#[derive(Debug)]
pub struct ChunkError {
    kind: ChunkErrorKind,
    description: String,
}

impl From<io::Error> for ChunkError {
    fn from(err: io::Error) -> Self {
        return match err.kind() {
            io::ErrorKind::NotFound => ChunkError::new(ChunkErrorKind::NotFound),
            _ => ChunkError::new(ChunkErrorKind::UnknownIo)
        };
    }
}

impl ChunkError {
    pub fn new(kind: ChunkErrorKind) -> Self {
        let description = match kind {
            ChunkErrorKind::NotFound => String::from("Chunk could not be found"),
            ChunkErrorKind::TooLarge => String::from("Chunk is too large to load"),
            ChunkErrorKind::UnknownIo => String::from("Unknown IO error"),
            ChunkErrorKind::UnsupportedFormat => String::from("This format is not supported"),
        };
        return ChunkError { description, kind };
    }
}

impl fmt::Display for ChunkError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for ChunkError {
    fn description(&self) -> &str {
        return &self.description;
    }
}

impl From<ChunkError> for CompilerError {
    fn from(error: ChunkError) -> Self {
        return CompilerError::new(CompilerErrorKind::ChunkError(error));
    }
}