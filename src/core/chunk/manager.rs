use core::{Chunk, ChunkId};

use std::sync::{Arc, Mutex};
use std::path::PathBuf;
use std::collections::{HashMap, HashSet};

#[derive(Debug,Default)]
pub struct ChunkManager {
    map: HashMap<ChunkId, Arc<Mutex<Chunk>>>,
    pub dirty_set: HashSet<ChunkId>
}

impl ChunkManager {
    pub fn dirty(&mut self, id: ChunkId) {
        self.dirty_set.insert(id);
    }

    pub fn undirty(&mut self) {
        self.dirty_set.clear();
    }

    pub fn get(&self, id: ChunkId) -> Arc<Mutex<Chunk>> {
        return self.map.get(&id).unwrap().clone();
    }

    pub fn contains(&self, id: ChunkId) -> bool {
        return self.map.contains_key(&id);
    }

    pub fn is_empty(&self) -> bool {
        return self.map.is_empty();
    }

    pub fn find(&self, path: &PathBuf) -> Option<ChunkId> {
        for (id, chunk) in self.map.iter() {
            match chunk.lock().unwrap().location.path {
                Some(ref chunk_path) => {
                    if chunk_path.eq(path) {
                        return Some(*id);
                    }
                }
                None => return None
            };
        }
        return None;
    }

    pub fn push(&mut self, chunk: Chunk) -> Arc<Mutex<Chunk>> {
        let id = chunk.id();
        self.dirty(id);
        let chunk = Arc::new(Mutex::new(chunk));
        let cloned = chunk.clone();
        self.map.insert(id, chunk);
        return cloned;
    }
}