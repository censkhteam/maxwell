use core::lexer::ident_table;
use core::{CompilerError, ChunkError, ChunkErrorKind, CompilerErrorKind};
use core::module::Dependencies;
use core::parser::{SyntaxTree, SourceLocation};

use std::fs::OpenOptions;
use std::io;
use std::io::{BufReader};
use std::io::prelude::*;
use std::path::PathBuf;

use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;

pub type ChunkId = u64;

#[derive(Debug, Clone)]
pub struct ChunkLocation {
    pub path: Option<PathBuf>,
    pub id: ChunkId,
}

impl ChunkLocation {
    pub fn is_virtual(&self) -> bool {
        return self.path.is_some();
    }

    pub fn absolute(path: PathBuf) -> Result<Self, ChunkError> {
        if !path.is_absolute() || !path.exists() {
            return Err(ChunkError::new(ChunkErrorKind::NotFound));
        }
        let id = ChunkLocation::generate_id(path.to_str().unwrap());
        return Ok(ChunkLocation {
            path: Some(path),
            id
        });
    }

    pub fn relative(parent: &ChunkLocation, mut path: PathBuf) -> Result<Self, ChunkError> {
        let from_dir = parent.try_path()?.parent().unwrap().to_owned();
        path = from_dir.join(path);
        if !path.is_absolute() || !path.exists() {
            return Err(ChunkError::new(ChunkErrorKind::NotFound));
        }
        let id = ChunkLocation::generate_id(path.to_str().unwrap());
        return Ok(ChunkLocation {
            path: Some(path),
            id
        });
    }

    pub fn of(source: &String) -> Self {
        return ChunkLocation { path: None, id: ChunkLocation::generate_id(source) };
    }

    fn generate_id(string: &str) -> ChunkId {
        let mut hasher = DefaultHasher::new();
        string.hash(&mut hasher);
        return hasher.finish();
    }

    pub fn id(&self) -> ChunkId {
        return self.id.clone();
    }

    pub fn try_path(&self) -> Result<&PathBuf, ChunkError> {
        return match self.path {
            Some(ref path) => { return Ok(path); }
            None => return Err(ChunkError::new(ChunkErrorKind::NotFound))
        };
    }
}

#[derive(Debug)]
pub struct Chunk {
    pub location: ChunkLocation,
    pub index: usize,
    pub source: String,
    pub syntax_tree: Option<SyntaxTree>,
    pub dependencies: Dependencies,
    pub raw_emit: bool,
}

impl<'a> Chunk {
    pub fn from_location(location: ChunkLocation) -> Self {
        return Chunk {
            location,
            source: String::new(),
            raw_emit: false,
            index: 0,
            syntax_tree: None,
            dependencies: Dependencies::new(),
        };
    }

    pub fn from_source(source: String) -> Self {
        let mut chunk = Chunk::from_location(ChunkLocation::of(&source));
        chunk.source = source;
        return chunk;
    }

    //TODO: This should not be a mut method
    pub fn pos(&self, index: usize) -> (usize, usize) {
        let (line_num, line) = self.source[..index]
            .lines()
            .enumerate()
            .last()
            .unwrap_or((0, ""));
        let col_num = line.chars().count();
        return (line_num, col_num);
    }

    pub fn id(&self) -> ChunkId {
        return self.location.id();
    }

    pub fn update_syntax_tree(&mut self, tree: SyntaxTree) {
        self.syntax_tree = Some(tree);
    }

    pub fn source_location(&mut self, index: usize) -> SourceLocation {
        return SourceLocation { index, id: self.id() };
    }

    pub fn current_source_location(&mut self) -> SourceLocation {
        let index = self.index;
        return self.source_location(index);
    }

    pub fn load_source(&mut self) -> Result<&str, ChunkError> {
        let mut file = OpenOptions::new()
            .create(false)
            .truncate(false)
            .read(true)
            .write(false)
            .open(self.location.try_path()?)?;

        if file.metadata()?.len() > 20000 {
            return Err(ChunkError::new(ChunkErrorKind::TooLarge));
        }

        let mut src = String::new();
        let mut reader = BufReader::new(file);
        reader.read_to_string(&mut src)?;

        //TODO: Do we need to copy the memory?
        self.source = src;
        return Ok(&self.source);
    }

    #[inline]
    pub fn peek_char(&self) -> char {
        return unsafe { *self.source.as_ptr().offset(self.index as isize) as char };
    }

    pub fn len(&self) -> usize {
        return self.source.len();
    }

    pub fn is_eof(&self) -> bool {
        return self.index >= self.len();
    }

    //TODO: This shouldn't be here.
    #[inline]
    pub fn consume_label(&mut self) -> &str {
        let start = self.index;
        //We bump the first character because we ASSUME it is an ident char if this method is being called.
        self.bump_char();

        loop {
            if self.is_eof() { break; }
            let char = self.peek_char();
            if ident_table::is_ident(char) {
                self.bump_char();
            } else {
                break;
            }
        }
        unsafe {
            return self.source.slice_unchecked(start, self.index);
        }
    }

    #[inline]
    pub fn slice(&'a self, start: usize, end: usize) -> &'a str {
        unsafe {
            return self.source.slice_unchecked(start, end);
        }
    }

    #[inline]
    pub fn bump_char(&mut self) {
        self.index += 1;
    }
}