extern crate term_painter;

use self::term_painter::ToStyle;
use self::term_painter::Color::*;

use core::log::Log;
use core::{Chunk, Compiler, ChunkId};

use std::sync::{Arc, Mutex};
use std::ops::Deref;
use std::time::{Duration, Instant};

pub struct CompileResult {
    pub overall_duration: Duration,
    pub emit_duration: Duration,
    pub parse_duration: Duration,
    pub setup_duration: Duration,
    pub plugin_duration: Duration,
    pub emit_count: i16,
    pub chunks: Vec<ChunkId>,
}

impl CompileResult {
    pub fn print(&self, compiler: &Compiler) {
        compiler.log.print(compiler);

        println!();
        println!("  {}", Green.paint(format!("Compiled {} chunks in {}", self.emit_count, self.duration_to_string(self.overall_duration))));
        println!("    {}", BrightBlack.paint(format!("Parsed in {}", self.duration_to_string(self.parse_duration))));
        println!();
        /*
        self.print_duration("Setup", self.setup_duration);
        self.print_duration("Parse", self.parse_duration);
        self.print_duration("Plugin", self.plugin_duration);
        self.print_duration("Emit", self.emit_duration);
    */
    }

    fn print_duration(&self, kind: &str, duration: Duration) {
        println!("{}", BrightBlack.paint(format!("  {:<7} | {:>14}", kind, self.duration_to_string(duration))));
    }

    fn duration_to_string(&self, duration: Duration) -> String {
        let mut time = duration.as_secs() as f64 * 1_000.0;
        time += duration.subsec_nanos() as f64 / 1_000_000.0;
        return format!("{:.7} ms", time);
    }
}