use core::output::{CompilerOutput, OutputResult, OutputError};
use core::{Compiler, Chunk};
use core::parser::Node;

use std::path::PathBuf;
use std::io::prelude::*;
use std::io;
use std::time::{Duration, Instant};
use std::fs;

#[derive(Debug)]
pub struct FileSystemOutput {
    pub out_dir: PathBuf,
    pub src_dir: PathBuf,
    writer: Option<io::BufWriter<fs::File>>,
}

impl FileSystemOutput {
    pub fn new(src_dir: PathBuf, out_dir: PathBuf) -> Self {
        return FileSystemOutput { src_dir, out_dir, writer: None };
    }

    fn get_out_path(&self, file_path: &PathBuf) -> PathBuf {
        let mut i = 0;
        loop {
            if file_path.components().nth(i).eq(&self.src_dir.components().nth(i)) {
                i += 1;
            } else {
                break;
            }
        }

        let mut out_path = self.out_dir.clone();
        loop {
            match file_path.components().nth(i) {
                Some(item) => {
                    out_path.push(item.as_os_str());
                    i += 1;
                }
                None => break,
            }
        }

        return out_path;
    }
}

impl CompilerOutput for FileSystemOutput {
    fn open(&mut self, chunk: &Chunk) -> Result<(), OutputError> {
        if self.writer.is_some() {
            panic!("File was not closed in generator");
        }

        let path = chunk.location.try_path()?;
        if !self.out_dir.exists() {
            fs::create_dir_all(self.out_dir.clone());
        }

        let file = fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(self.get_out_path(path))?;

        self.writer = Some(io::BufWriter::new(file));
        return Ok(());
    }

    fn close(&mut self) -> Result<(), OutputError> {
        if self.writer.is_none() {
            panic!("Trying to close empty generator");
        }
        match self.writer {
            Some(ref mut writer) => {writer.flush();},
            _ => {}
        };
        self.writer = None;
        return Ok(());
    }

    fn write_string(&mut self, string: &str) {
        match self.writer {
            Some(ref mut writer) => {
                writer.write(string.as_bytes());
            }
            _ => {}
        };
    }
}