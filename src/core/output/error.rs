use std::io::Error as IoError;
use std::error::Error;
use std::convert::From;
use std::fmt;

use core::{CompilerError,CompilerErrorKind, ChunkError};


#[derive(Debug)]
pub struct OutputError {
    description: String,
}

impl OutputError {
    pub fn new(description: String) -> Self {
        return OutputError { description };
    }
}

impl Error for OutputError {
    fn description(&self) -> &str {
        return &self.description;
    }
}

impl fmt::Display for OutputError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description)
    }
}

impl From<ChunkError> for OutputError {
    fn from(error: ChunkError) -> Self {
        return OutputError::new(error.description().to_owned());
    }
}

impl From<IoError> for OutputError {
    fn from(error: IoError) -> Self {
        return OutputError::new(error.description().to_owned());
    }
}

impl From<OutputError> for CompilerError {
    fn from(x: OutputError) -> Self {
        return CompilerError::new(CompilerErrorKind::OutputError(x));
    }
}