use core::{Compiler,Chunk};
use core::output::OutputError;

use std::time::Duration;
use std::default::Default;
use std::fmt::Debug;

pub struct OutputResult {
    pub duration: Duration,
}

pub trait CompilerOutput {
    fn open(&mut self, chunk : &Chunk) -> Result<(), OutputError>;

    fn close(&mut self) -> Result<(), OutputError>;

    fn write_string(&mut self, string: &str);
}