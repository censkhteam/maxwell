use core::parser::ParserError;
use core::output::OutputError;
use core::{LoadError,Compiler, ChunkError};
use core::log::{Message,Severity};

use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct CompilerError {
    pub kind: CompilerErrorKind,
    description: String,
}

impl CompilerError {
    pub fn new(kind: CompilerErrorKind) -> Self {
        return CompilerError { description: kind.to_string(), kind };
    }
}

impl fmt::Display for CompilerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for CompilerError {
    fn description<'a>(&'a self) -> &'a str {
        return &self.description;
    }
}

#[derive(Debug)]
pub enum CompilerErrorKind {
    InvalidConfig(LoadError),
    ParserError(ParserError),
    OutputError(OutputError),
    ChunkError(ChunkError),
    NoEntries
}

impl Message for CompilerError {
    fn severity(&self) -> Severity {
        return Severity::Error;
    }

    fn content(&self, session: &Compiler) -> String {
        return match self.kind {
            CompilerErrorKind::ParserError(ref parser_error) => parser_error.content(session),
            _ => self.description().to_owned()
        }
    }
}

impl ToString for CompilerErrorKind {
    fn to_string(&self) -> String {
        use self::CompilerErrorKind::*;
        use self::LoadError::*;

        match *self {
            InvalidConfig(ref config_err) => config_err.to_string(),
            OutputError(ref output_err) => output_err.description().to_owned(),
            ParserError(ref parser_err) => parser_err.description().to_owned(),
            ChunkError(ref chunk_error) => chunk_error.description().to_owned(),
            NoEntries => String::from("No entries defined")
        }
    }
}