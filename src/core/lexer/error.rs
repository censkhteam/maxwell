use core::parser::{ParserError, ParserErrorKind, SourceRange};

use std::error::Error;
use std::fmt;
use std::convert::From;

#[derive(Debug)]
pub struct TokenizerError {
    description: String,
    pub range: SourceRange,
}

impl fmt::Display for TokenizerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Tokenizer Error: {}", self.description())
    }
}

impl TokenizerError {
    pub fn new(description: String, range: SourceRange) -> Self {
        return TokenizerError { description, range };
    }
}

impl Error for TokenizerError {
    fn description(&self) -> &str {
        return &self.description;
    }
}

impl From<TokenizerError> for ParserError {
    fn from(error: TokenizerError) -> Self {
        return ParserError::new(ParserErrorKind::Syntax(format!("{}", error)), error.range);
    }
}