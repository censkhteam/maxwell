use std::fmt;


pub trait TokenKind: Clone {
    fn to_string(&self) -> String;

    fn is_new_line(&self) -> bool;

    fn is_whitespace(&self) -> bool {
        return self.get_whitespace() > 0;
    }

    fn get_whitespace(&self) -> u8;
}