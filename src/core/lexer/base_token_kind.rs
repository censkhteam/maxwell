use super::TokenKind;

use self::BaseTokenKind::*;

#[derive(Debug, PartialEq, Clone)]
pub enum BaseTokenKind {
    EndOfFile,
    Whitespace(u8),
    Newline,
}

impl TokenKind for BaseTokenKind {
    fn is_new_line(&self) -> bool {
        return match *self {
            Newline => true,
            _ => false
        };
    }

    fn get_whitespace(&self) -> u8 {
        return match *self {
            Whitespace(count) => count,
            _ => 0
        };
    }

    fn to_string(&self) -> String {
        return match *self {
            EndOfFile => String::new(),
            Whitespace(ref count) => {
                let mut str = String::new();
                for _ in 0..*count {
                    str += " ";
                }
                return str;
            }
            Newline => String::from("\r\n"),
        };
    }
}