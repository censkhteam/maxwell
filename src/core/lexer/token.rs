use core::lexer::TokenKind;
use core::parser::SourceRange;
use core::Chunk;

use std::ops::Deref;

#[derive(Debug, Clone)]
pub struct Token<T: TokenKind> {
    kind: Box<T>,
    pub prefix: String,
    pub start: usize,
    pub end: usize,
}

impl<T: TokenKind> Token<T> {
    pub fn range(&self, chunk: &mut Chunk) -> SourceRange {
        return SourceRange::new(chunk.source_location(self.start), chunk.source_location(self.end));
    }
}

impl<T: TokenKind> Token<T> {
    pub fn new(kind: T, start: usize, end: usize) -> Self {
        return Token {
            kind: Box::new(kind),
            prefix: String::new(),
            start,
            end,
        };
    }

    pub fn kind(&self) -> T {
        return (*self.kind.deref()).clone();
    }
}