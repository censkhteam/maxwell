use super::{TokenKind, TokenizerError, Token, BaseTokenKind};

use core::Chunk;
use core::parser::QuoteKind;

pub trait Tokenizer<T: TokenKind> {
    fn pop_token(&self, chunk: &mut Chunk) -> Result<Token<T>, TokenizerError>;

    #[inline]
    fn peek_token(&self, chunk: &mut Chunk) -> Result<Token<T>, TokenizerError> {
        let start_index = chunk.index;
        let result = self.pop_token(chunk);
        chunk.index = start_index;
        return result;
    }

    #[inline]
    fn pop_base_token(&self, chunk: &mut Chunk) -> Result<Option<BaseTokenKind>, TokenizerError> {
        if chunk.is_eof() {
            return Ok(Some(BaseTokenKind::EndOfFile));
        }

        let char = chunk.peek_char();

        if char == '\n' {
            chunk.bump_char();
            return Ok(Some(BaseTokenKind::Newline));
        }

        if char == '\r' {
            chunk.bump_char();
            if !chunk.is_eof() && chunk.peek_char() == '\n' {
                chunk.bump_char();
            }
            return Ok(Some(BaseTokenKind::Newline));
        }

        if char.is_whitespace() {
            let mut count = 1;
            chunk.bump_char();
            loop {
                if chunk.is_eof() {
                    break;
                }
                if chunk.peek_char() == '\n' || chunk.peek_char() == '\r' {
                    break;
                }
                if chunk.peek_char().is_whitespace() {
                    count += 1;
                    chunk.bump_char();
                } else {
                    break;
                }
            }
            return Ok(Some(BaseTokenKind::Whitespace(count)));
        }
        return Ok(None);
    }

    #[inline]
    fn pop_ignore_whitespace<'a>(&self, chunk: &'a mut Chunk) -> Result<Token<T>, TokenizerError> {
        let mut string = String::new();
        let mut token: Token<T>;

        loop {
            token = self.pop_token(chunk)?;
            if token.kind().is_whitespace() {
                for _ in 0..token.kind().get_whitespace() {
                    string += " ";
                }
            } else {
                break;
            }
        }
        token.prefix = string;

        return Ok(token);
    }

    #[inline]
    fn peek_ignore_whitespace<'a>(&self, chunk: &'a mut Chunk) -> Result<Token<T>, TokenizerError> {
        let start_index = chunk.index;
        let result = self.pop_ignore_whitespace(chunk);
        chunk.index = start_index;
        return result;
    }

    #[inline]
    fn peek_ignore_padding<'a>(&self, chunk: &'a mut Chunk) -> Result<Token<T>, TokenizerError> {
        let start_index = chunk.index;
        let result = self.pop_ignore_padding(chunk);
        chunk.index = start_index;
        return result;
    }

    #[inline]
    fn pop_ignore_padding<'a>(&self, chunk: &'a mut Chunk) -> Result<Token<T>, TokenizerError> {
        let mut string = String::new();
        let mut token: Token<T>;

        loop {
            token = self.pop_token(chunk)?;
            if token.kind().is_new_line() {
                string += "\n";
            } else if token.kind().is_whitespace() {
                for _ in 0..token.kind().get_whitespace() {
                    string += " ";
                }
            } else {
                break;
            }
        }
        token.prefix = string;

        return Ok(token);
    }

    #[inline]
    fn read_quote_until<'a>(&self, chunk: &'a mut Chunk, quote_char: char) -> (String, QuoteKind) {
        let start = chunk.index + 1;

        chunk.bump_char();

        loop {
            match chunk.peek_char() {
                '\\' => {
                    chunk.bump_char();
                    chunk.bump_char();
                }
                char => {
                    if char == quote_char { break; }
                    chunk.bump_char();
                }
            }
        }

        let end = chunk.index;
        let value = chunk.slice(start, end).to_owned();
        chunk.bump_char();

        let quote = match quote_char {
            '"' => QuoteKind::SpeechMark,
            '\'' => QuoteKind::Apostrophe,
            _ => panic!("Invalid char")
        };
        return (value, quote);
    }
}