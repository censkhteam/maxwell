use std::sync::mpsc::{channel, Sender, Receiver, RecvError};
use core::{Event,EventKind, Compiler};

pub type Listener = (Fn(Event)) + Send;

pub struct EventBus {
    sender: Sender<EventKind>,
    receiver: Receiver<EventKind>,
    listeners: Vec<Box<Listener>>,
}

impl EventBus {
    pub fn new() -> Self {
        let (sender, receiver) = channel();

        return EventBus {
            sender,
            receiver,
            listeners: Vec::new(),
        };
    }

    pub fn listen(&mut self, listener: Box<Listener>) {
        self.listeners.push(listener);
    }

    pub fn channel(&self) -> Sender<EventKind> {
        return self.sender.clone();
    }

    pub fn handle<'a>(&mut self, compiler : &mut Compiler) {
        match self.receiver.recv() {
            Ok(ref kind) => {
                for listener in &self.listeners {
                    let result = listener(Event::new(compiler, kind));
                }
            }
            Err(err) => {
                eprintln!("{:?}", err);
            }
        };
    }
}