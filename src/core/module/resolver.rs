use core::{Chunk, ChunkLocation, Compiler};
use core::log::{Log, Message, Severity};

use std::collections::HashSet;
use std::path::PathBuf;

#[derive(Debug)]
pub enum ResolverError {
    ImportNotFound(String)
}

impl Message for ResolverError {
    fn severity(&self) -> Severity {
        return Severity::Error;
    }

    fn content(&self, session: &Compiler) -> String {
        return match *self {
            ResolverError::ImportNotFound(ref location) => format!("Could not resolver chunk at location '{}'", location)
        };
    }
}

pub struct Resolver {}

impl Resolver {
    pub fn resolve(&self, chunk: &Chunk, requires: &HashSet<String>, log: &mut Log) -> Result<Vec<ChunkLocation>, ResolverError> {
        let mut new_chunks = Vec::new();
        for import in requires {
            match self.resolve_import(chunk, import) {
                Ok(location) => new_chunks.push(location),
                Err(err) => {
                    let mut new_import = import.clone();
                    new_import += ".js";
                    match self.resolve_import(chunk, &new_import) {
                        Ok(location) => new_chunks.push(location),
                        Err(err) => {
                            log.add_message(chunk.id(), Box::new(err));
                        }
                    }
                }
            }
        }
        return Ok(new_chunks);
    }


    fn resolve_import(&self, chunk: &Chunk, import: &String) -> Result<ChunkLocation, ResolverError> {
        let location = ChunkLocation::relative(&chunk.location, PathBuf::from(import));
        if location.is_ok() {
            return Ok(location.unwrap());
        }
        return Err(ResolverError::ImportNotFound(import.clone()));
    }
}