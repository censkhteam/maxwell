use core::Compiler;
use super::{Plugin, PluginPass};
use super::super::parser::Node;

use std::ops::DerefMut;
use std::time::{Duration, Instant};

#[derive(Debug)]
pub struct ApplyResult {
    pub duration: Duration,
}

#[derive(Debug)]
pub enum ApplyError {}

pub struct PluginManager {
    pub plugins: Vec<Box<Plugin>>
}

impl PluginManager {
    pub fn new() -> Self {
        return PluginManager {
            plugins: Vec::new(),
        };
    }

    pub fn apply(&self, compiler: &mut Compiler) -> Result<ApplyResult, ApplyError> {
        let start = Instant::now();

        for id in compiler.chunk_manager.dirty_set.clone() {
            let chunk_mutex = compiler.chunk_manager.get(id);
            let mut chunk = chunk_mutex.lock().unwrap();
            match chunk.syntax_tree {
                Some(ref mut tree) => {
                    self.walk_nodes(tree.head.deref_mut());
                }
                None => {}
            }
        }

        return Ok(ApplyResult { duration: start.elapsed() });
    }

    fn walk_nodes(&self, node: &mut Node) -> Result<(), ApplyError> {
        for plugin in &self.plugins {
            for pass in node.plugin_passes().iter_mut() {
                plugin.handle(pass);
            }
        }
        match node.children() {
            Some(ref mut children) => {
                for child in children.iter_mut() {
                    self.walk_nodes(*child);
                }
            }
            _ => {}
        }

        return Ok(());
    }

    pub fn add_plugin<T>(&mut self, plugin: T) where T: Plugin + 'static {
        self.plugins.push(Box::new(plugin));
    }
}
