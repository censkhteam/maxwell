use super::super::Chunk;

use js::{StatementNode, ExpressionNode};

use std::error::Error;
use std::hash::{Hash, Hasher};
use std::fmt::Debug;

pub trait Plugin: Debug {
    fn handle(&self, pass: &mut PluginPass) -> Result<(), Box<Error>>;

    fn get_name(&self) -> &str;
}

impl Hash for Plugin {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.get_name().hash(state);
    }
}

impl Eq for Plugin {}

impl PartialEq for Plugin {
    fn eq(&self, other: &Plugin) -> bool {
        return self.get_name().eq(other.get_name());
    }
}

pub enum PluginPass<'a> {
    StatementNode(&'a mut StatementNode),
    ExpressionNode(&'a mut ExpressionNode),
}