use core::log::Severity;
use core::Compiler;

use std::error::Error;
use std::fmt::Debug;
use std::ops::Deref;

pub trait Message: Send + Sync + Debug {
    fn severity(&self) -> Severity;
    fn content(&self, session: &Compiler) -> String;
}

#[derive(Default, Debug, Clone)]
pub struct LogItem {
    content: String,
    severity: Severity,
}

impl LogItem {
    pub fn new(severity: Severity, content: String) -> Self {
        return LogItem { severity, content };
    }
}

impl Message for LogItem {
    fn severity(&self) -> Severity {
        return self.severity.clone();
    }

    fn content(&self, compiler: &Compiler) -> String {
        return self.content.clone();
    }
}

