extern crate term_painter;

use self::term_painter::ToStyle;
use self::term_painter::Color::*;

use core::{Compiler, ChunkId};
use core::log::{Message, Severity};

use std::iter::Filter;
use std::slice::Iter;
use std::collections::HashMap;

#[derive(Debug, Default)]
pub struct Log {
    map: HashMap<ChunkId, Vec<Box<Message>>>,
}

impl Log {
    pub fn print(&self, compiler: &Compiler) {
        for (id, messages) in self.map.iter() {
            for message in messages {
                let prefix = message.severity().color().paint(format!("[{}]", message.severity().kind()));
                println!("{} {}", prefix, message.content(compiler));
            }
        }
    }

    pub fn clear_all(&mut self, id: ChunkId) {
        self.map.clear();
    }

    pub fn clear(&mut self, id: ChunkId) {
        self.get_messages(id).clear();
    }

    pub fn absorb(&mut self, other: Log) {
        for (id, messages) in other.map.into_iter() {
            for message in messages {
                self.add_message(id, message);
            }
        }
    }

    pub fn get_messages(&mut self, id: ChunkId) -> &mut Vec<Box<Message>> {
        if self.map.contains_key(&id) {
            return self.map.get_mut(&id).unwrap();
        }
        self.map.insert(id, Vec::new());
        return self.map.get_mut(&id).unwrap();
    }

    pub fn add_message(&mut self, id: ChunkId, message: Box<Message>) {
        self.get_messages(id).push(message);
    }
}