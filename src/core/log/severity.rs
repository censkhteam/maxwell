extern crate term_painter;

use self::term_painter::ToStyle;
use self::term_painter::Color;
use self::term_painter::Style;

#[derive(Debug, PartialEq, Clone)]
pub enum Severity {
    Info,
    Debug,
    Warning,
    Error
}

impl Default for Severity {
    fn default() -> Self {
        return Severity::Info;
    }
}

impl Severity {
    pub fn color(&self) -> Style {
        use self::Severity::*;

        return match *self {
            Info => Color::White.to_style(),
            Debug => Color::White.dim(),
            Warning => Color::Yellow.to_style(),
            Error => Color::Red.to_style()
        };
    }

    pub fn kind(&self) -> &'static str {
        use self::Severity::*;

        return match *self {
            Info => "info",
            Debug => "debug",
            Warning => "warning",
            Error => "error"
        };
    }
}