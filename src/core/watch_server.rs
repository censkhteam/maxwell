extern crate notify;

use self::notify::{Watcher, RecommendedWatcher, RecursiveMode, watcher, DebouncedEvent};

extern crate term_painter;

use self::term_painter::ToStyle;
use self::term_painter::Color::*;

use core::{ChunkId, Event, EventKind, EventBus};

use std::thread;
use std::sync::{Mutex, Arc};

use std::sync::mpsc::{channel, Sender, Receiver, RecvError};
use std::time::Duration;
use std::path::{Path, PathBuf};

pub struct WatchServer {
    files: Vec<PathBuf>,
    pub event_bus: EventBus,
    pub sender: Option<Sender<DebouncedEvent>>
}

impl WatchServer {
    pub fn new() -> Self {
        return WatchServer {
            sender: None,
            files: Vec::default(),
            event_bus: EventBus::new()
        };
    }

    pub fn init(&mut self) {
        let (sender, receiver) = channel();
        let event_channel = self.event_bus.channel();
        self.sender = Some(sender.clone());
        thread::spawn(move || {
            let mut watcher = watcher(sender, Duration::from_millis(50)).unwrap();
            loop {
                match receiver.recv() {
                    Ok(notice) => {
                        match notice {
                            DebouncedEvent::Create(path) => {
                                watcher.watch(path, RecursiveMode::Recursive);
                            }
                            DebouncedEvent::Write(ref path) => {
                                event_channel.send(EventKind::FileChanged(PathBuf::from(path)));
                                //let chunk = session.chunk_manager.find(&PathBuf::from(path)).unwrap();
                            }
                            _ => {}
                        }
                    }
                    Err(err) => {
                        println!("{}", Red.paint(format!("Watch Error: {:?}", err)));
                    }
                };
            }
        });
    }
}