use core::{CompileResult, ChunkId, Compiler};

use std::path::{Path, PathBuf};

pub enum EventKind {
    Compilation(CompileResult),
    FileChanged(PathBuf),
    ChunkChanged(ChunkId)
}

pub struct Event<'a> {
    pub compiler: &'a mut Compiler,
    pub kind: &'a EventKind,
}

impl<'a> Event<'a> {
    pub fn new(compiler: &'a mut Compiler, kind: &'a EventKind) -> Self {
        return Event { compiler, kind };
    }
}