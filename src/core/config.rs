use core::ChunkLocation;
use core::output::{CompilerOutput};

use std::path::PathBuf;

#[derive(Default)]
pub struct CompilerConfig {
    pub entries: Vec<ChunkLocation>,
    pub plugins: Vec<String>,
    pub output: Option<Box<CompilerOutput>>,

    pub out_dir: PathBuf,
    pub working_dir: PathBuf,
    pub src_dir: PathBuf,

    pub watch: bool,
}