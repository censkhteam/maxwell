use core::ChunkId;

//TODO: Source range should only know about usize and not SourceLocation

#[derive(Debug, PartialEq, Clone, Default, Copy)]
pub struct SourceRange {
    pub start: SourceLocation,
    pub end: SourceLocation,
}

impl SourceRange {
    pub fn new(start: SourceLocation, end: SourceLocation) -> Self {
        return SourceRange { start, end };
    }

    pub fn chunk_id(&self) -> ChunkId {
        return self.start.id;
    }
}

#[derive(Debug, PartialEq, Clone, Default, Copy)]
pub struct SourceLocation {
    pub index: usize,
    pub id: ChunkId,
    //pub line_num: u16,
    //pub col_num: u16,
}

impl SourceLocation {}