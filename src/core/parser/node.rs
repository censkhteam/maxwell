use core::transform::PluginPass;
use core::parser::{SourceLocation, SourceRange};
use core::output::CompilerOutput;

use std::fmt::Debug;

pub type NodeList<'a> = Vec<&'a mut Node>;

pub trait Node: Debug + Send + Sync {
    fn generate(&self, output: &mut Box<CompilerOutput>);

    fn children<'a>(&'a mut self) -> Option<NodeList<'a>>;

    fn range<'a>(&'a self) -> &'a SourceRange;

    fn plugin_passes(&mut self) -> Vec<PluginPass>;
}

#[derive(Debug, PartialEq, Clone, Default)]
pub struct NodeTrivia {
    pub prefix: String,
    pub suffix: String,
}