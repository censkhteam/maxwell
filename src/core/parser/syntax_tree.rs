use core::parser::Node;
use core::transform::PluginPass;
use core::output::CompilerOutput;

#[derive(Debug)]
pub struct SyntaxTree {
    pub head: Box<Node>,
}

impl SyntaxTree {
    pub fn new(head: Box<Node>) -> Self {
        return SyntaxTree { head };
    }

    pub fn generate(&self, output : &mut Box<CompilerOutput>){
        self.head.generate(output);
    }
}