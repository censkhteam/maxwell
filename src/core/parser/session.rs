use core::lexer::{Tokenizer, TokenKind};
use core::parser::SourceLocation;
use core::{Chunk};
use core::log::Log;

use std::collections::HashSet;

pub struct ParserSession<'a, T: TokenKind> {
    pub tokenizer: Box<Tokenizer<T>>,
    pub log: Log,
    pub chunk: &'a mut Chunk,
    pub requires: HashSet<String>,

}

impl<'a, T: TokenKind> ParserSession<'a, T> {
    pub fn new(chunk: &'a mut Chunk, tokenizer: Box<Tokenizer<T>>) -> Self {
        return ParserSession {
            log: Log::default(),
            chunk,
            tokenizer,
            requires: HashSet::new(),
        };
    }

    pub fn source_location(&mut self, index: usize) -> SourceLocation {
        return self.chunk.source_location(index);
    }

    pub fn current_source_location(&mut self) -> SourceLocation {
        return self.chunk.current_source_location();
    }
}