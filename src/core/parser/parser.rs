use core::{Chunk, ChunkId};
use core::transform::PluginManager;
use core::lexer::{TokenKind, Tokenizer};
use core::parser::{SyntaxTree, ParserError, ParserSession};
use core::log::Log;

use std::collections::HashSet;
use std::time::Duration;
use std::fmt::Debug;

#[derive(Debug)]
pub struct ParserResult {
    pub requires: HashSet<String>,
    pub log: Log,
    pub duration: Duration,
    pub chunks: HashSet<ChunkId>,
}

impl ParserResult {
    pub fn new<'a, T: TokenKind>(session: ParserSession<'a, T>, duration: Duration) -> Self {
        let chunk = session.chunk.id();
        let log = session.log;
        let requires = session.requires;

        let mut set = HashSet::new();
        set.insert(chunk);

        return ParserResult {
            requires,
            log,
            duration,
            chunks: set
        };
    }
}

pub trait Parser: Debug + Sync + Send {
    fn name(&self) -> &'static str;

    fn query_extension(&self, string: &str) -> bool;

    fn parse(&self, chunk: &mut Chunk) -> Result<ParserResult, ParserError>;
}