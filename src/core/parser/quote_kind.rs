#[derive(Debug, PartialEq, Clone)]
pub enum QuoteKind {
    SpeechMark,
    Apostrophe
}