extern crate term_painter;

use self::term_painter::ToStyle;
use self::term_painter::Color::*;

use core::parser::{SourceLocation, SourceRange};
use core::{CompilerError, CompilerErrorKind, Compiler};
use core::log::{Message, Severity};

use std::error::Error;
use std::fmt;

#[derive(Debug, Clone)]
pub enum ParserErrorKind {
    Syntax(String),
    Setup(String),
    Unimplemented,
}

impl ToString for ParserErrorKind {
    fn to_string(&self) -> String {
        use self::ParserErrorKind::*;

        match *self {
            Syntax(ref string) => { string.clone() }
            Setup(ref string) => { string.clone() }
            Unimplemented => { String::from("Not implemented yet ") }
        }
    }
}

#[derive(Debug, Clone)]
pub struct ParserError {
    kind: ParserErrorKind,
    description: String,
    range: SourceRange,
}

impl fmt::Display for ParserError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for ParserError {
    fn description(&self) -> &str {
        return &self.description;
    }
}

impl ParserError {
    pub fn new(kind: ParserErrorKind, range: SourceRange) -> Self {
        return ParserError { description: kind.to_string(), kind, range };
    }
}

impl From<ParserError> for CompilerError {
    fn from(x: ParserError) -> Self {
        return CompilerError::new(CompilerErrorKind::ParserError(x));
    }
}

impl Message for ParserError {
    fn severity(&self) -> Severity {
        return Severity::Error;
    }

    fn content(&self, compiler: &Compiler) -> String {
        let chunk_mutex = compiler.chunk_manager.get(self.range.chunk_id());
        let chunk = chunk_mutex.lock().unwrap();
        let (line_num, col_num) = chunk.pos(self.range.start.index);
        let mut path = chunk.location.try_path().unwrap().strip_prefix(&compiler.config.working_dir).unwrap().to_str().unwrap().to_owned();
        path = path.trim_left_matches("\\\\?\\").to_owned();
        let file_info = format!("   --> ({},{}) {}", line_num, col_num, path);
        return format!("{}\n{}", self.description(), BrightBlack.paint(file_info));
    }
}