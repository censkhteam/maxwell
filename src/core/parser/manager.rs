use super::Parser;
use core::lexer::TokenKind;

use std::path::PathBuf;

#[derive(Debug)]
pub enum Error {
    NoExtension
}

pub struct ParserManager {
    pub parsers: Vec<Box<Parser>>
}

impl ParserManager {
    pub fn new() -> Self {
        return ParserManager { parsers: Vec::new() };
    }

    pub fn parser_from_path(&self, path: &PathBuf) -> Result<&Box<Parser>, Error> {
        return match path.extension() {
            Some(extension) => {
                for parser in &self.parsers {
                    if parser.query_extension(extension.to_str().unwrap()) {
                        return Ok(parser);
                    }
                }
                Err(Error::NoExtension)
            }
            None => Err(Error::NoExtension)
        };
    }
}