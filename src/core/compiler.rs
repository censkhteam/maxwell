extern crate notify;

use self::notify::DebouncedEvent;

use html::HtmlParser;

use js::{JsParser, MinifyPlugin};

use core::*;
use core::module::Resolver;
use core::parser::{ParserError, ParserErrorKind, ParserResult, ParserManager, Parser, SourceLocation};
use core::transform::PluginManager;
use core::log::Log;
use core::parser::SyntaxTree;

use std::sync::mpsc::Sender;
use std::ops::{Deref, DerefMut};
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};
use std::collections::HashSet;

pub struct Compiler {
    resolver: Resolver,
    parser_manager: ParserManager,
    pub config: CompilerConfig,
    pub chunk_manager: ChunkManager,
    pub log: Log,
    pub event_sender: Option<Sender<EventKind>>
}

impl Compiler {
    pub fn new(config: CompilerConfig) -> Self {
        let mut parser_manager = ParserManager::new();
        parser_manager.parsers.push(Box::new(JsParser::new()));
        parser_manager.parsers.push(Box::new(HtmlParser::new()));

        let mut compiler = Compiler {
            resolver: Resolver {},
            parser_manager,
            config,
            chunk_manager: ChunkManager::default(),
            log: Log::default(),
            event_sender: None
        };

        compiler.init();

        return compiler;
    }

    fn compile(&mut self) -> Result<(), CompilerError> {
        let start = Instant::now();

        if self.chunk_manager.is_empty() {
            return Err(CompilerError::new(CompilerErrorKind::NoEntries));
        }

        let setup_duration = start.elapsed();

        let mut chunks = Vec::new();

        for id in self.chunk_manager.dirty_set.clone() {
            self.log.clear(id);
            match self.parse_id(id) {
                Err(err) => {
                    self.log.add_message(id, Box::new(err));
                }
                Ok(mut parsed_result) => {
                    for id in parsed_result.chunks {
                        chunks.push(id);
                    }
                    self.log.absorb(parsed_result.log);
                }
            }
        };

        let parse_duration = start.elapsed();

        let mut plugin_manager = PluginManager::new();
        plugin_manager.add_plugin(MinifyPlugin {});

        let plugin_result = plugin_manager.apply(self);
        let plugin_duration = plugin_result.unwrap().duration;

        let mut result = CompileResult {
            setup_duration,
            plugin_duration,
            overall_duration: Duration::default(),
            parse_duration,
            emit_duration: Duration::default(),
            emit_count: 0,
            chunks,
        };
        match self.config.output {
            Some(ref mut output) => {
                let mut count = 0;
                let start = Instant::now();

                for id in &self.chunk_manager.dirty_set {
                    let chunk_mutex = self.chunk_manager.get(*id);
                    let mut chunk = chunk_mutex.lock().unwrap();
                    output.open(chunk.deref_mut())?;
                    match chunk.syntax_tree {
                        Some(ref mut syntax_tree) => {
                            syntax_tree.generate(output);
                        }
                        _ => {}
                    };
                    if chunk.syntax_tree.is_none() {
                        output.write_string(&chunk.source);
                    }
                    output.close()?;
                    count += 1;
                }
                result.emit_duration = start.elapsed();
                result.emit_count = count;
            }
            _ => {}
        }
        self.chunk_manager.undirty();
        result.overall_duration = start.elapsed();
        match self.event_sender {
            Some(ref sender) => {
                sender.send(EventKind::Compilation(result));
            }
            None => {}
        }
        return Ok(());
    }

    fn init(&mut self) -> Result<(), CompilerError> {
        if self.config.entries.is_empty() {
            return Err(CompilerError::new(CompilerErrorKind::NoEntries));
        }
        for location in &self.config.entries.clone() {
            self.chunk(location);
        }
        return Ok(());
    }

    pub fn watch(&mut self) -> Result<WatchServer, CompilerError> {
        let mut watch_server = WatchServer::new();
        self.event_sender = Some(watch_server.event_bus.channel());
        watch_server.init();
        let watch_sender = watch_server.sender.clone().unwrap();
        {
            match self.compile() {
                _ => {}
                Err(err) => return Err(err)
            };
        }
        {
            let events_channel = watch_server.event_bus.channel();
            watch_server.event_bus.listen(Box::new(move |e: Event| {
                match *e.kind {
                    EventKind::FileChanged(ref path) => {
                        let id = e.compiler.chunk_manager.find(path).unwrap();
                        events_channel.send(EventKind::ChunkChanged(id));
                    }
                    EventKind::ChunkChanged(ref id) => {
                        e.compiler.chunk_manager.dirty(*id);
                        e.compiler.compile();
                    }
                    EventKind::Compilation(ref result) => {
                        for id in &result.chunks {
                            let path = {
                                let chunk_mutex = e.compiler.chunk_manager.get(*id);
                                let chunk = chunk_mutex.lock().unwrap();
                                chunk.location.try_path().unwrap().clone()
                            };
                            watch_sender.send(DebouncedEvent::Create(path));
                        }
                    }
                    _ => {}
                };
            }));
        }
        return Ok(watch_server);
    }

    fn parse_id(&mut self, id: ChunkId) -> Result<ParserResult, CompilerError> {
        let chunk_mutex = self.chunk_manager.get(id);
        let (id, mut parse_result) = {
            let mut chunk_value = chunk_mutex.lock().unwrap();
            let chunk = chunk_value.deref_mut();
            chunk.load_source()?;
            let parser = self.parser(chunk)?;
            (chunk.id(), parser.parse(chunk)?)
        };
        self.parse_requires(id, &mut parse_result);
        return Ok(parse_result);
    }

    fn parse_requires(&mut self, id: ChunkId, result: &mut ParserResult) -> Result<(), CompilerError> {
        let chunk_mutex = self.chunk_manager.get(id);
        let mut chunk = chunk_mutex.lock().unwrap();
        let locations = self.resolver.resolve(chunk.deref(), &result.requires, &mut result.log).unwrap();
        for location in &locations {
            chunk.dependencies.requires.push(location.id());
            result.chunks.insert(location.id());
            let child_chunk = self.chunk(location);
            child_chunk.lock().unwrap().dependencies.used_by.push(id);
            if self.chunk_manager.dirty_set.contains(&location.id()) {
                match self.parse_id(location.id()) {
                    Ok(sub_result) => {
                        result.log.absorb(sub_result.log);
                    }
                    Err(err) => {
                        result.log.add_message(location.id(), Box::new(err));
                    }
                }
            }
        }
        return Ok(());
    }

    fn chunk<'a>(&mut self, location: &ChunkLocation) -> Arc<Mutex<Chunk>> {
        let id = location.id();
        if !self.chunk_manager.contains(id) {
            let chunk = Chunk::from_location(location.clone());
            return self.chunk_manager.push(chunk);
        }
        return self.chunk_manager.get(id);
    }

    fn parser(&self, chunk: &Chunk) -> Result<&Box<Parser>, CompilerError> {
        return match chunk.location.path {
            Some(ref path) => {
                match self.parser_manager.parser_from_path(path) {
                    Ok(parser) => Ok(parser),
                    Err(err) => Err(CompilerError::from(ChunkError::new(ChunkErrorKind::UnsupportedFormat)))
                }
            }
            None => Err(CompilerError::from(ChunkError::new(ChunkErrorKind::UnsupportedFormat)))
        };
    }
}
